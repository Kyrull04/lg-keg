
def get_admins(admin_ref):
    """
    Obtiene la lista de administradores de la base de datos.
    """
    try:
        # Obtiene los datos de los administradores desde la referencia proporcionada
        admins = admin_ref.get()
        
        # Crea una lista de diccionarios con la información de los administradores
        list_admin = [{'user': admin.get('user'), 
                       'nick': admin.get('nick'), 
                       'role': admin.get('role')
                      }
                      for admin in admins]
        
        # Retorna la lista de administradores
        return list(list_admin)
    except Exception as e:
        # Captura cualquier excepción que ocurra al acceder a la colección de administradores
        print(f'Error al acceder a la colección admins: {e}')
        # Retorna una lista vacía en caso de error
        return []
    




def get_clients(client_ref):
    """
    Obtiene la lista de clientes de la base de datos.

    """
    try:
        # Obtiene los datos de los clientes desde la referencia proporcionada
        clients = client_ref.get()
        
        # Crea una lista de diccionarios con la información de los clientes
        list_client = [{'name': client.get('name'), 
                        'id': client.get('id'), 
                        'phone': client.get('phone'),
                        'email': client.get('email'),
                        'address': client.get('address')
                       }
                       for client in clients]
        
        # Retorna la lista de clientes
        return list(list_client)
    except Exception as e:
        # Captura cualquier excepción que ocurra al acceder a la colección de clientes
        print(f'Error al acceder a la colección clients: {e}')
        # Retorna una lista vacía en caso de error
        return []




def get_barrels(barrels_ref):
    """
    Obtiene la lista de barriles de la base de datos.
    """
    try:
        # Obtiene los datos de los barriles desde la referencia proporcionada
        barrels = barrels_ref.get()
        
        # Retorna la lista de barriles
        return list(barrels)
    except Exception as e:
        # Captura cualquier excepción que ocurra al acceder a la colección de barriles
        print(f'Error al acceder a la colección barrels: {e}')
        # Retorna una lista vacía en caso de error
        return []
    



def get_contracts(contracts_ref):
    """
    Obtiene la lista de contratos de la base de datos.
    """
    try:
        # Obtiene los datos de los contratos desde la referencia proporcionada
        contracts = contracts_ref.get()
        
        # Crea una lista de diccionarios con la información de los contratos
        list_contract = [{'barrel_id': contract.get('barrel_id'), 
                          'client': contract.get('client'), 
                          'client_id': contract.get('client_id'),
                          'end_date': contract.get('end_date'),
                          'init_date': contract.get('init_date'),
                          'condition': contract.get('condition')
                         }
                         for contract in contracts]
        
        # Retorna la lista de contratos
        return list(list_contract)
    except Exception as e:
        # Captura cualquier excepción que ocurra al acceder a la colección de contratos
        print(f'Error al acceder a la colección clients: {e}')
        # Retorna una lista vacía en caso de error
        return []





def show_clients(client_ref):
    """
    Obtiene y muestra los clientes de la base de datos.
    """
    clients = client_ref.get()
    return [{'name': client.get('name'), 'id': client.id} if client.get('name') else {'name': ''} for client in clients]



def show_liters(l):
    """
    Obtiene y muestra las cantidades de litros de la base de datos.
    """
    liters = l.get()
    return [{'quantity': liter.get('quantity')} if liter.get('quantity') else {'quantity': ''} for liter in liters]


def show_styles(s):
    """
    Obtiene y muestra los estilos de la base de datos.
    """
    styles = s.get()
    return [{'name': style.get('name')} if style.get('name') else {'name': ''} for style in styles]



def show_batchs(b):
    """
    Obtiene y muestra los lotes de la base de datos.
    """
    batchs = b.get()
    return [{'batch': batch.get('batch')} if batch.get('batch') else {'batch': ''} for batch in batchs]