# Importaciones de bibliotecas externas
import datetime as dt
from datetime import datetime, timedelta
from flask import session

# Importaciones de módulos internos de la aplicación
import gets_and_show



def barrel_exist(id, ref):
    """
    Busca un barril con un ID específico en una referencia dada.

    Esta función asume que "ref" es una instancia válida de una colección o referencia a una base de datos en Firebase Firestore.

    """
    
    # Realiza una consulta a la referencia para obtener los barriles con el ID especificado
    barrel = ref.where('id', '==', id).get()

    # Itera sobre los documentos encontrados (debería ser solo uno)
    for doc in barrel:
        # Obtiene la referencia al documento encontrado
        doc_ref = ref.document(f'{doc.id}')
 
    # Retorna el documento encontrado (si existe) y su referencia
    return doc, doc_ref
  
    
    
def get_barrels_data(barrel_id, ref):
    """
    Obtiene los datos de un barril específico en una referencia dada.

    Esta función asume que "ref" es una instancia válida de una colección o referencia a un documento en Firebase Firestore.
    """
    
    # Obtiene la referencia al documento del barril utilizando su ID
    doc_ref = ref.document(barrel_id)

    # Obtiene el documento
    doc = doc_ref.get()

    # Verifica si el documento existe
    if doc.exists:
        # Convierte el documento a un diccionario y lo devuelve
        return doc.to_dict()
    else:
        # Si el documento no existe, devuelve None
        return None
    



def get_data_by_id(id, barrels_ref):
    """
    Otiene los datos de un barril específico en una referencia dada utilizando su ID.

    """
    # Realiza una consulta a la referencia para obtener los barriles con el ID especificado
    href = barrels_ref.where('id', '==', id).get()

    # Verifica si se encontró algún barril con el ID especificado
    if len(href) > 0:
        # Obtiene el primer documento de los resultados de la consulta
        barrel_doc = href[0]

        # Convierte el documento a un diccionario
        barrel_data = barrel_doc.to_dict()

        # Retorna los datos del barril encontrado
        return barrel_data
    else:
        # Si no se encuentra ningún barril con el ID especificado, retorna None
        return None





def quick_update(id, location, barrels_ref):
    """
    Esta función actualiza rápidamente la ubicación de un barril específico en una referencia dada utilizando su ID.

    """
    # Realiza una consulta a la referencia para obtener los barriles con el ID especificado
    data = barrels_ref.where('id', '==', id).get()

    # Verifica si se encontró algún barril con el ID especificado
    if len(data) > 0:
        # Obtiene la referencia al documento del barril encontrado
        data_ref = data[0].reference

        # Actualiza la ubicación del barril en la base de datos
        data_ref.update({'location': location})




def quick_update_by_condition(id, condition, barrels_ref):
    """
    Actualiza rápidamente la condición de un barril específico en una referencia dada utilizando su ID.

    """
    # Realiza una consulta a la referencia para obtener los barriles con el ID especificado
    data = barrels_ref.where('id', '==', id).get()

    # Verifica si se encontró algún barril con el ID especificado
    if len(data) > 0:
        # Obtiene la referencia al documento del barril encontrado
        data_ref = data[0].reference

        # Actualiza la condición del barril en la base de datos
        data_ref.update({'condition': condition})




def get_style_of_batch(s):
    """
    Extrae el estilo de un lote dado en formato de cadena.

    Esta función asume que el estilo en la cadena está separado por comas y se encuentra en la segunda posición después de dividir la cadena.
    """
    # Divide la cadena en partes utilizando la coma como separador
    parts = s.split(',')

    # Verifica si hay más de una parte en la cadena (es decir, si hay estilo)
    if len(parts) > 1:
        # Devuelve el estilo (se elimina cualquier espacio en blanco adicional)
        return parts[1].strip()
    else:
        # Si no se encuentra ningún estilo, devuelve una cadena vacía
        return ''




def validate_field(value):
    """
    Valida si un valor dado contiene solo caracteres permitidos.

    """
    # Definir el conjunto de caracteres permitidos
    allowed_characters = set('abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789-')
    
    # Iterar sobre cada carácter en el valor dado
    for char in value:
        # Verificar si el carácter no está en el conjunto de caracteres permitidos
        if char not in allowed_characters:
            # Si encuentra un carácter no permitido, devuelve False
            return False
    
    # Si todos los caracteres son permitidos, devuelve True
    return True




def cycle_counter(ref):
    """
    Cuenta el número de veces que la palabra "Cambia" aparece en la lista 
    de acciones en 'action' de una referencia dada.
    """
    # Inicializamos el contador
    count = 0
    
    # Recorremos la lista de acciones en 'action'
    for action in ref['action']:
        # Verificamos si la palabra "Cambia" está en action
        if 'Cambia' in action:
            # Incrementamos el contador si encontramos "Cambia"
            count += 1
    
    # Devolvemos el contador que representa el número de veces que "Cambia" aparece en la lista de acciones
    return count





def average_time_state(barrel):
    """
    Calcula el tiempo promedio que un barril ha pasado en 
    cada estado basado en su historial de condiciones.

    """
    # Extrae los estados del barril
    estados = barrel['conditions_history']
    # Extrae las fechas del barril
    fechas = barrel['date']
    
    # Convierte las fechas de entrada en objetos datetime
    fechas_datetime = [dt.datetime.strptime(fecha, '%d/%m/%Y  %H:%M') for fecha in fechas]
    
    # Inicializa un diccionario para almacenar el tiempo total en cada estado y el número de transiciones a ese estado
    tiempo_en_estado = {}
    
    # Itera sobre cada estado y fecha
    for i in range(len(estados)):
        estado_actual = estados[i]
        fecha_actual = fechas_datetime[i]
        
        # Verifica si el estado actual no está en el diccionario y agrega una entrada si es necesario
        if estado_actual not in tiempo_en_estado:
            tiempo_en_estado[estado_actual] = {'tiempo_total': timedelta(0), 'contador': 0}
        
        # Verifica si hay un estado siguiente para calcular la diferencia de tiempo entre estados
        if i < len(estados) - 1:
            estado_siguiente = estados[i + 1]
            fecha_siguiente = fechas_datetime[i + 1]
            
            # Calcula la diferencia de tiempo entre los estados y actualiza el tiempo total en el estado actual
            tiempo_en_estado[estado_actual]['tiempo_total'] += (fecha_siguiente - fecha_actual)
            tiempo_en_estado[estado_actual]['contador'] += 1
    
    # Calcula el tiempo promedio en cada estado en días, horas y minutos
    tiempo_promedio_por_estado = {}
    for estado, datos_tiempo in tiempo_en_estado.items():
        tiempo_total = datos_tiempo['tiempo_total']
        contador = datos_tiempo['contador']
        
        # Calcula el tiempo promedio en días, horas y minutos
        tiempo_promedio_dias = tiempo_total.days / contador if contador > 0 else 0
        tiempo_promedio_segundos = tiempo_total.seconds / contador if contador > 0 else 0
        tiempo_promedio_horas = tiempo_promedio_segundos // 3600
        tiempo_promedio_minutos = (tiempo_promedio_segundos % 3600) // 60
        tiempo_promedio_por_estado[estado] = (int(tiempo_promedio_dias), int(tiempo_promedio_horas), int(tiempo_promedio_minutos))
    
    return tiempo_promedio_por_estado




def verify_cycle_counter(id, barrels_ref, ref):
    """
    Verifica el contador de ciclos de un barril, y si supera un umbral especificado, 
    realiza ciertas acciones en la base de datos.

    """
    # Realiza una consulta para obtener los barriles con el ID especificado
    href = barrels_ref.where('id', '==', id).stream()
    # Convierte el resultado de la consulta en una lista
    barrel_docs = list(href)
    
    # Verifica si se encontró exactamente un documento con el ID especificado
    if len(barrel_docs) == 1:
        # Obtiene el primer documento de la lista y lo convierte en un diccionario
        barrel_doc = barrel_docs[0].to_dict()

        # Calcula el contador de ciclos
        cc = cycle_counter(ref)
        
        # Verifica si el valor de change_cycle es mayor que 250
        if cc > 250:
            # Elimina el primer elemento de las listas 'conditions_history', 'date', 'action' y 'user'
            barrel_doc['conditions_history'].pop(0)
            barrel_doc['date'].pop(0)
            barrel_doc['action'].pop(0)
            barrel_doc['user'].pop(0)
            # Actualiza el documento en Firestore
            barrels_ref.document(barrel_docs[0].id).update(barrel_doc)
        
            # Mensaje de éxito
            session['success_msg'] = "Documento actualizado en Firestore"
       
        # Verifica si el historial de lotes es mayor que 250
        if len(barrel_doc['batchs_history']) > 250:
            # Elimina el primer elemento de las listas 'batchs_history' y 'batchs_date'
            barrel_doc['batchs_history'].pop(0)
            barrel_doc['batchs_date'].pop(0)

    else:
        # Mensaje de error si no se encuentra exactamente un documento con el ID especificado
        session['error_msg'] = "El documento no existe en Firestore o hay múltiples documentos con el mismo ID"





def count_barrels(barrels, condition):
    """
    Cuenta los barriles que cumplen con una condición específica y calcula el total de litros y 
    el número de barriles gasificados y no gasificados por estado.

    """
    # Inicializa el contador de barriles y litros
    count = 0
    liters = 0
    # Inicializa el diccionario para almacenar el número de barriles gasificados y no gasificados por estado
    gasified = {"En servicio": {"gasified": 0, "not_gasified": 0},
                "En almacenamiento": {"gasified": 0, "not_gasified": 0}}
  
    # Itera sobre cada barril en la lista
    for barrel in barrels:
        # Verifica si el barril tiene la condición especificada
        if barrel.get('condition') == condition:
            # Incrementa el contador de barriles
            count += 1
            # Suma los litros del barril al total
            liters += int(barrel.get('liters'))

            # Verifica si el estado está presente en el diccionario 'gasified'
            if condition in gasified:
                # Actualiza el conteo de barriles gasificados y no gasificados según la columna 'gasified'
                if barrel.get('gasified') == 'SI':
                    gasified[condition]['gasified'] += 1
                else:
                    gasified[condition]['not_gasified'] += 1
            else:
                # Si el estado no está presente en el diccionario, inicializa sus valores en cero
                gasified[condition] = {'gasified': 0, 'not_gasified': 0}
                # Actualiza el conteo de barriles gasificados y no gasificados según la columna 'gasified'
                if barrel.get('gasified') == 'SI':
                    gasified[condition]['gasified'] += 1
                else:
                    gasified[condition]['not_gasified'] += 1
         
    return count, liters, gasified





def process_barrels(barrels_ref, barrels=0):
    """
    Procesa los datos de los barriles y genera estadísticas y 
    listas de barriles para su visualización.

    """
    # Si no se proporciona una lista de barriles, obtén los barriles de la referencia
    if barrels == 0:
        barrels = gets_and_show.get_barrels(barrels_ref)
    
    # Verifica si la lista de barriles está vacía
    if not barrels:
        # Si no hay barriles, devuelve tuplas vacías y listas vacías
        return (0, 0, 0, 0, 0, 0, 0, 0), (0, 0, 0, 0, 0, 0), [], [], [], [], []
    else: 
        # Calcula el número de barriles, litros y la distribución de barriles por estado
        service, service_liters, gas_service = count_barrels(barrels, 'En servicio')
        stored, stored_liters, gas_stored = count_barrels(barrels, 'En almacenamiento')
        empty, empty_liters, gas_empty = count_barrels(barrels, 'Vacío')
        dirt, dirt_liters, gas_dirty = count_barrels(barrels, 'Sucio')

        # Calcula el total de barriles, barriles útiles, total de litros y litros netos
        count = service + stored + empty + dirt
        util_barrels = service + stored
        total_liters = service_liters + stored_liters + empty_liters + dirt_liters
        net_liters = service_liters + stored_liters
   
        # Calcula el total de barriles gasificados y no gasificados
        total_gasified = gas_service['En servicio']['gasified'] + gas_stored['En almacenamiento']['gasified']
        total_no_gasified = gas_service['En servicio']['not_gasified'] + gas_stored['En almacenamiento']['not_gasified']
        
        # Crea una lista de barriles con información detallada
        list_barrel = [{'id': barrel.get('id'), 
                        'liters': barrel.get('liters'), 
                        'serial': barrel.get('serial'),
                        'batch': barrel.get('batch'),
                        'batchs_history': barrel.get('batchs_history'),
                        'batchs_date': barrel.get('batchs_date'),
                        'condition': barrel.get('condition'), 
                        'style': barrel.get('style'),
                        'location': barrel.get('location'), 
                        'gasified': barrel.get('gasified'),
                        'conditions_history': barrel.get('conditions_history'),
                        'date': barrel.get('date'),
                        'user': barrel.get('user')}
                    for barrel in barrels]

        # Calcula la cantidad de barriles por estilo y por lote
        barrel_count_style = count_barrels_by(barrels, by_style=True)
        barrel_count_batch = count_barrels_by(barrels, by_style=False)
        
        # Calcula la cantidad de barriles por ubicación y condición
        barrel_location = count_barrels_by_location(barrels)
        service_location= count_barrels_by_location_and_condition(barrels, 'En servicio')
        storage_location = count_barrels_by_location_and_condition(barrels, 'En almacenamiento')
        dirty_location = count_barrels_by_location_and_condition(barrels, 'Vacío')
        empty_location = count_barrels_by_location_and_condition(barrels, 'Sucio')
        location_condition = [service_location, storage_location, dirty_location, empty_location]
        
        # Devuelve los resultados calculados
        return (service, stored, empty, dirt, count, util_barrels, total_gasified,  total_no_gasified), (service_liters, stored_liters,
                                                   empty_liters, dirt_liters, total_liters, net_liters), list_barrel, barrel_count_style, barrel_count_batch, barrel_location, location_condition





def count_barrels_by_location(barrels):
    """
    Calcula la cantidad de barriles por ubicación, excluyendo la fábrica y el bar, 
    y suma los litros de los barriles en cada ubicación.

    """
    # Inicializa un diccionario para almacenar el recuento de barriles y litros por ubicación
    barrel_location_count = {}

    # Itera sobre cada barril en la lista
    for barrel in barrels:
        # Obtiene la ubicación del barril
        location = barrel.get('location')
        # Si la ubicación no es la fábrica ni el bar, se considera que está en manos del cliente
        if location != 'Fábrica' and location != 'Bar':
            location = 'Cliente' 
        # Obtiene los litros del barril
        liters = int(barrel.get('liters'))

        # Verifica si la ubicación ya está en el diccionario y actualiza el recuento de barriles y litros
        if location in barrel_location_count:
            barrel_location_count[location]['count'] += 1
            barrel_location_count[location]['liters'] += liters
        else:
            # Si la ubicación no está en el diccionario, la agrega con el recuento de barriles y litros del barril actual
            barrel_location_count[location] = {
                'count': 1,
                'liters': liters
            }
    
    # Devuelve el diccionario con el recuento de barriles y litros por ubicación
    return barrel_location_count



def count_barrels_by_location_and_condition(barrels, condition):
    """
    Calcula la cantidad de barriles por ubicación y condición, 
    excluyendo la fábrica y el bar, y suma los litros de los barriles en 
    cada ubicación y condición.

    """
    # Inicializa un diccionario para almacenar el recuento de barriles y litros por ubicación y condición
    barrel_location_condition_count = {}

    # Itera sobre cada barril en la lista
    for barrel in barrels:
        # Obtiene la ubicación del barril
        location = barrel.get('location')
        # Si la ubicación no es la fábrica ni el bar, se considera que está en manos del cliente
        if location != 'Fábrica' and location != 'Bar':
            location = 'Cliente' 
        # Obtiene los litros del barril
        liters = int(barrel.get('liters'))

        # Verifica si la condición del barril coincide con la condición proporcionada
        if barrel.get('condition') == condition:
            # Verifica si la ubicación ya está en el diccionario y actualiza el recuento de barriles y litros
            if location in barrel_location_condition_count:
                barrel_location_condition_count[location]['count'] += 1
                barrel_location_condition_count[location]['liters'] += liters
            else:
                # Si la ubicación no está en el diccionario, la agrega con el recuento de barriles y litros del barril actual
                barrel_location_condition_count[location] = {
                    'count': 1,
                    'liters': liters
                }
    
    # Devuelve el diccionario con el recuento de barriles y litros por ubicación y condición
    return barrel_location_condition_count



def count_barrels_by_style(barrels):
    """
    Calcula la cantidad de barriles por estilo y realiza 
    subcálculos relacionados con litros, estado y gasificación.

    """
    # Inicializa un diccionario para almacenar el recuento de barriles por estilo y sus características relacionadas
    barrel_count = {}
    
    # Itera sobre cada barril en la lista
    for barrel in barrels:
        # Obtiene el estilo del barril
        style = barrel.get('style')
        # Verifica si el estilo está vacío o no está presente y pasa al siguiente barril si es así
        if style is None or style == '':
            continue 
        # Obtiene los litros del barril
        liters = int(barrel.get('liters'))
        # Verifica si el barril está gasificado
        gasified = barrel.get('gasified') == 'SI'
        # Verifica si el barril está en servicio
        in_service = barrel.get('condition') in ['En servicio']
        # Verifica si el barril está en almacenamiento
        in_storage = barrel.get('condition') in ['En almacenamiento']
        
        # Verifica si el estilo no está presente en el diccionario y agrega una entrada si es necesario
        if style not in barrel_count:
            barrel_count[style] = {
                'count': 0,
                'liters': 0,
                'net_count': 0,
                'net_liters': 0,
                'gasified': 0,
                'gasified_liters': 0,
                'not_gasified': 0,
                'not_gasified_liters': 0,
                'service_style': 0,
                'storage_style': 0
            }
        
        # Actualiza los recuentos y los litros según las características del barril
        barrel_count[style]['count'] += 1
        barrel_count[style]['liters'] += liters
        
        if in_service:
            barrel_count[style]['service_style'] += 1
        
        if in_storage:
            barrel_count[style]['storage_style'] += 1
        
        if gasified:
            barrel_count[style]['gasified'] += 1
            barrel_count[style]['gasified_liters'] += liters
        
        if not gasified:
            barrel_count[style]['not_gasified'] += 1
            barrel_count[style]['not_gasified_liters'] += liters
          
    # Devuelve el diccionario con el recuento de barriles por estilo y sus características relacionadas
    return barrel_count



def count_barrels_by_batch(barrels):
    """
    Calcula la cantidad de barriles por lote y realiza 
    subcálculos relacionados con litros, estado y gasificación.

    """
    # Inicializa un diccionario para almacenar el recuento de barriles por lote y sus características relacionadas
    barrel_count = {}
    
    # Itera sobre cada barril en la lista
    for barrel in barrels:
        # Obtiene el lote del barril
        batch = barrel.get('batch')
        # Verifica si el lote está vacío o no está presente y pasa al siguiente barril si es así
        if batch is None or batch == '':
            continue

        # Obtiene los litros del barril
        liters = int(barrel.get('liters'))
        # Verifica si el barril está gasificado
        gasified = barrel.get('gasified') == 'SI'
        # Verifica si el barril está en servicio
        in_service = barrel.get('condition') in ['En servicio']
        # Verifica si el barril está en almacenamiento
        in_storage = barrel.get('condition') in ['En almacenamiento']

        # Verifica si el lote no está presente en el diccionario y agrega una entrada si es necesario
        if batch not in barrel_count:
            barrel_count[batch] = {
                'count': 0,
                'liters': 0,
                'net_count': 0,
                'net_liters': 0,
                'gasified': 0,
                'gasified_liters': 0,
                'not_gasified': 0,
                'not_gasified_liters': 0,
                'service_batch': 0,
                'storage_batch': 0
            }
        
        # Actualiza los recuentos y los litros según las características del barril
        barrel_count[batch]['count'] += 1
        barrel_count[batch]['liters'] += liters
        
        if in_service:
            barrel_count[batch]['service_batch'] += 1
        
        if in_storage:
            barrel_count[batch]['storage_batch'] += 1
        
        if gasified:
            barrel_count[batch]['gasified'] += 1
            barrel_count[batch]['gasified_liters'] += liters
        
        if not gasified:
            barrel_count[batch]['not_gasified'] += 1
            barrel_count[batch]['not_gasified_liters'] += liters
          
    # Devuelve el diccionario con el recuento de barriles por lote y sus características relacionadas
    return barrel_count




def count_barrels_by(barrels, by_style=True):
    """
    Calcula la cantidad de barriles por estilo o lote y 
    realiza subcálculos relacionados con litros, estado y gasificación.

    """
    # Inicializa un diccionario para almacenar el recuento de barriles por estilo o lote y sus características relacionadas
    barrel_count = {}

    # Itera sobre cada barril en la lista
    for barrel in barrels:
        # Determina la categoría (estilo o lote) del barril según el argumento by_style
        category = barrel.get('style') if by_style else barrel.get('batch')

        # Verifica si la categoría está vacía o no está presente y pasa al siguiente barril si es así
        if category is None or category == '':
            continue

        # Obtiene los litros del barril
        liters = int(barrel.get('liters'))
        # Verifica si el barril está gasificado
        gasified = barrel.get('gasified') == 'SI'
        # Verifica si el barril está en servicio
        in_service = barrel.get('condition') in ['En servicio']
        # Verifica si el barril está en almacenamiento
        in_storage = barrel.get('condition') in ['En almacenamiento']

        # Verifica si la categoría no está presente en el diccionario y agrega una entrada si es necesario
        if category not in barrel_count:
            barrel_count[category] = {
                'count': 0,
                'liters': 0,
                'net_count': 0,
                'net_liters': 0,
                'gasified': 0,
                'gasified_liters': 0,
                'not_gasified': 0,
                'not_gasified_liters': 0,
                'service_category': 0,
                'storage_category': 0
            }

        # Actualiza los recuentos y los litros según las características del barril
        barrel_count[category]['count'] += 1
        barrel_count[category]['liters'] += liters

        if in_service:
            barrel_count[category]['service_category'] += 1

        if in_storage:
            barrel_count[category]['storage_category'] += 1

        if gasified:
            barrel_count[category]['gasified'] += 1
            barrel_count[category]['gasified_liters'] += liters

        if not gasified:
            barrel_count[category]['not_gasified'] += 1
            barrel_count[category]['not_gasified_liters'] += liters

    # Devuelve el diccionario con el recuento de barriles por estilo o lote y sus características relacionadas
    return barrel_count




def calculate_percentage(response):
    """
    Calcula el porcentaje de los primeros cuatro elementos de 
    la lista en relación con el quinto elemento (total).

    """
    # Verifica si el quinto elemento (total) es 0 o no está presente
    if not response[4]:
        # Devuelve una lista de ceros si el total es 0 o no está presente
        return [0] * 4
    else:
        # Obtiene el total
        total = int(response[4])
        # Calcula y redondea los porcentajes de los primeros cuatro elementos en relación con el total
        return [round(int(r) / total * 100) for r in response[:4]]



def percentage(barrels_ref):
    """
    Calcula el porcentaje de diversos aspectos 
    relacionados con los barriles y sus datos.

    """
    # Obtiene diversas respuestas y datos relacionados con los barriles mediante el proceso de barriles
    response1, response2, list_barrel, count_barrels, barrel_count_batch, barrel_location, location_condition = process_barrels(barrels_ref)
    
    # Calcula los porcentajes basados en la primera respuesta
    p1, p2, p3, p4 = calculate_percentage(response1)
    
    # Calcula los porcentajes basados en la segunda respuesta
    l1, l2, l3, l4 = calculate_percentage(response2)
    
    # Devuelve una lista que contiene los porcentajes calculados
    return [p1, p2, p3, p4, l1, l2, l3, l4]





def buscar_barriles_en_rango(fecha_inicio, fecha_fin, barrels_ref):
    """
    Busca los barriles cuyas fechas estén dentro del rango especificado 
    y realiza modificaciones en los datos encontrados.

    """
    # Convierte las fechas a objetos datetime si no lo están
    if not isinstance(fecha_inicio, datetime):
        fecha_inicio = datetime.strptime(fecha_inicio, '%Y-%m-%d %H:%M')
    if not isinstance(fecha_fin, datetime):
        fecha_fin = datetime.strptime(fecha_fin, '%Y-%m-%d %H:%M')
    
    # Lista para almacenar los barriles encontrados dentro del rango
    barriles_encontrados = []
    
    # Obtiene los barriles de la base de datos
    barrels = gets_and_show.get_barrels(barrels_ref)

    # Itera sobre cada barril obtenido
    for resultado in barrels:
        # Convierte el resultado a un diccionario
        barril = resultado.to_dict()
        
        # Verifica si el campo 'date' está presente y es una lista
        if 'date' in barril and isinstance(barril['date'], list):
            # Listas para almacenar datos coincidentes
            fechas_coincidentes = []
            conditions_history_coincidente = []
            
            # Itera sobre cada fecha en el campo 'date'
            for fecha_str in barril['date']:
                # Convierte la fecha de string a objeto datetime
                fecha = datetime.strptime(fecha_str.split()[0], '%d/%m/%Y')
                # Verifica si la fecha está dentro del rango especificado
                if fecha_inicio <= fecha <= fecha_fin:
                    # Agrega la fecha coincidente a la lista
                    fechas_coincidentes.append(fecha_str)
                    # Obtiene el índice de la fecha coincidente en 'date'
                    index_fecha = barril['date'].index(fecha_str)
                    # Agrega el valor correspondiente en 'conditions_history' a la lista
                    conditions_history_coincidente.append(barril['conditions_history'][index_fecha])
                    
            # Verifica si hay fechas coincidentes
            if fechas_coincidentes:
                # Actualiza el campo 'date' con las fechas coincidentes
                barril['date'] = fechas_coincidentes
                # Actualiza el campo 'conditions history' con los valores coincidentes
                barril['conditions_history'] = conditions_history_coincidente
                # Elimina claves no necesarias
                keys_a_eliminar = ['gasified', 'action', 'user']
                for key in keys_a_eliminar:
                    if key in barril:
                        del barril[key]
                # Agrega el conteo de elementos en 'conditions_history'
                barril['conditions_history_count'] = len(conditions_history_coincidente)
                # Agrega el barril modificado a la lista de barriles encontrados
                barriles_encontrados.append(barril)

    # Devuelve la lista de barriles encontrados dentro del rango especificado
    return barriles_encontrados
