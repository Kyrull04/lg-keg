# Importaciones de la biblioteca estándar de Python
import base64
from io import BytesIO

# Importaciones de bibliotecas externas
import matplotlib.pyplot as plt


def draw_bar_graphics(estado_contador):
    plt.close('all')
    porcentajes = 0

    # Colores personalizados para las barras
    color_cantidad = '#63cb9a'  # Verde
    color_porcentaje = '#4579b2'  # Azul

    # Calcular el total de elementos en el diccionario
    total_elementos = sum(estado_contador.values())
    # Recorrer el diccionario y calcular el porcentaje de cada estado
    porcentajes = {estado: (cantidad / total_elementos) * 100 for estado, cantidad in estado_contador.items()}
   

    estados = ['Sucio', 'Vacío', 'En servicio', 'En almacenamiento']


     # Crear gráfico de barras apiladas con diseño mejorado
    fig, ax = plt.subplots(figsize=(10, 6))

    # Barra para la cantidad de estados
    bar1 = ax.bar(estados, [estado_contador[estado] for estado in estados], color=color_cantidad, edgecolor='black', label='Cantidad de Estados')

    # Barra para el porcentaje
    bar2 = ax.bar(estados, [porcentajes[estado] for estado in estados], bottom=[estado_contador[estado] for estado in estados], color=color_porcentaje, edgecolor='black', label='Frecuencia de cambios')

    ax.set_ylabel('Cantidad / Porcentaje', fontsize=12, fontweight='bold')
    ax.set_title('Estados y Frecuencia', fontsize=14, fontweight='bold')

    # Agregar etiquetas con valores numéricos
    for rect, valor in zip(bar1 + bar2, [estado_contador[estado] for estado in estados] + [porcentajes[estado] for estado in estados]):
        height = rect.get_height()
        if rect in bar2:  # Mostrar solo el número decimal y el signo de porcentaje en las etiquetas de porcentaje
            ax.annotate('{:.1f}%'.format(valor),
                        xy=(rect.get_x() + rect.get_width() / 2, height),
                        xytext=(0, -15),  # offset vertical
                        textcoords="offset points",
                        ha='center', va='center',
                        fontsize=10, fontweight='bold', color='white')  # Estilo mejorado para porcentaje
        else:  # Mostrar solo el valor de cantidad en las etiquetas de cantidad
            ax.annotate('{}'.format(valor),
                        xy=(rect.get_x() + rect.get_width() / 2, height),
                        xytext=(0, 5),  # offset vertical
                        textcoords="offset points",
                        ha='center', va='center',
                        fontsize=10, fontweight='bold')  # Estilo mejorado para cantidad

    ax.legend(fontsize=10, loc='upper left')

    # Ajustes adicionales para mejorar el diseño
    ax.spines['top'].set_visible(False)
    ax.spines['right'].set_visible(False)
    ax.spines['left'].set_linewidth(0.5)
    ax.spines['bottom'].set_linewidth(0.5)
    ax.tick_params(axis='both', which='both', length=0)

    # Guardar el gráfico en un objeto BytesIO
    img_buf = BytesIO()
    plt.savefig(img_buf, format='png', bbox_inches='tight')
    img_buf.seek(0)

    # Convertir la imagen a base64 para incrustarla en el template
    img_base64 = base64.b64encode(img_buf.getvalue()).decode('utf-8')


    return img_base64
