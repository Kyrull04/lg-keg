# Importaciones de la biblioteca estándar de Python
import datetime as dt
from datetime import datetime, timedelta

# Importaciones de bibliotecas externas
import ntplib

# Importaciones de módulos internos de la aplicación
from flask import request




def get_time():
    """
    Obtiene la fecha y hora actual desde un servidor NTP
    """
    servidores_ntp = ['ar.pool.ntp.org', 'south-america.pool.ntp.org']
    c = ntplib.NTPClient()
   
    while True:
        for servidor_ntp in servidores_ntp:
            try:
                response = c.request(servidor_ntp)
                
                date = dt.datetime.fromtimestamp(response.tx_time)
                format_date = date.strftime('%d/%m/%Y  %H:%M')
                return format_date
            except ntplib.NTPException as e:
                # Si ocurre una excepción, intenta con el siguiente servidor NTP
                print(f"Error de conexión con el servidor NTP {servidor_ntp}: {e}")
        
        # Si todos los servidores fallan, obtén la fecha del sistema y sale del bucle
        system_date = dt.datetime.now()
        format_system_date = system_date.strftime('%d/%m/%Y  %H:%M')
        print(f"Error: no se pudo obtener la fecha y hora desde NTP, utilizando la fecha del sistema: {format_system_date}")
        return format_system_date
    











def get_new_date(current, days_to_add):
    """
    Devuelve una fecha y hora actualizada sumando una cierta cantidad de días a la fecha actual
    """
    current_date = current
    date_obj = dt.datetime.strptime(current_date, '%d/%m/%Y %H:%M')
    new_date_obj = date_obj + timedelta(days= int(days_to_add))
    new_date = new_date_obj.strftime('%d/%m/%Y  %H:%M')
    return new_date





# Función para calcular las fechas según la opción seleccionada
def calcular_fechas(opcion):
    """
    Calcula las fechas de inicio y fin según la opción seleccionada.

    """
    # Obtiene la fecha y hora actual
    fecha_actual = datetime.now()

    # Inicializa las variables para las fechas de inicio y fin
    fecha_inicio = None
    fecha_fin = None

    # Calcula las fechas según la opción seleccionada
    if opcion == '1-semana':
        fecha_inicio = fecha_actual - timedelta(days=7)
    elif opcion == '2-semanas':
        fecha_inicio = fecha_actual - timedelta(days=14)
    elif opcion == '3-semanas':
        fecha_inicio = fecha_actual - timedelta(days=21)
    elif opcion == '4-semanas':
        fecha_inicio = fecha_actual - timedelta(days=28)
    elif opcion == '1-mes':
        fecha_inicio = fecha_actual - timedelta(days=30)
    elif opcion == 'rango-fechas':
        # Obtiene las fechas de inicio y fin ingresadas manualmente
        fecha_inicio_str = request.form['fecha-inicio']
        fecha_fin_str = request.form['fecha-fin']

        # Convierte las fechas de string a objetos datetime
        fecha_inicio = datetime.strptime(fecha_inicio_str, '%d/%m/%Y')
        fecha_fin = datetime.strptime(fecha_fin_str, '%d/%m/%Y')

    # Formatea las fechas en el formato deseado (YYYY-MM-DD HH:MM)
    fecha_inicio = fecha_inicio.strftime('%Y-%m-%d %H:%M') if fecha_inicio else None
    fecha_fin = fecha_fin.strftime('%Y-%m-%d %H:%M') if fecha_fin else None

    return fecha_inicio, fecha_fin


