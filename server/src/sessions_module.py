# Importaciones de módulos internos de la aplicación
from flask import request



def get_role_id(value):
    """
    Obtiene el ID del rol a partir de un valor dado.
    """
    if value:
        aux = value.split("session__")
        if len(aux) > 1:
            role_id = aux[1][0]
            return role_id
        else:
            return None



def get_sessions(session_ref):
    """
    Obtiene la lista de sesiones de la base de datos.

    """
    try:
        # Obtiene los datos de las sesiones desde la referencia proporcionada
        sessions = session_ref.get()
        
        # Crea una lista de diccionarios con la información de las sesiones
        list_sessions = [{'last_login': session.get('last_login'), 
                          'logged_in': session.get('logged_in'), 
                          'logout': session.get('logout'),
                          'username': session.get('username'),
                         }
                         for session in sessions]
        
        # Retorna la lista de sesiones
        return list(list_sessions)
    except Exception as e:
        # Captura cualquier excepción que ocurra al acceder a la colección de sesiones
        print(f'Error al acceder a la colección session: {e}')
        # Retorna una lista vacía en caso de error
        return []



def get_cookie(request):
    """
    Obtiene la sesión almacenada en las cookies de la solicitud.   
    """
    session = request.cookies.get('__session')
    return session




def get_admin_of_session(value):
    
    """
    Retorna una cadena de texto sin el prefijo 'session__' y el primer caracter inmediatamente después de esta expresión.
    """
    if not isinstance(value, str) or not value.startswith('session__'):
        return None
    
    return value[len('session__'):][1:]




