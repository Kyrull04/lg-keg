from flask import Flask, render_template, request, redirect, session, jsonify, url_for, make_response
from uuid import uuid4
import jwt
import re 
import firebase_admin
from firebase_admin import credentials
from firebase_admin import firestore
import functions_module
from datetime import datetime, timedelta

import pandas as pd



app = Flask(__name__)

cred = credentials.Certificate("keys.json")
firebase_admin.initialize_app(cred)
db=firestore.client()

barrels_ref = db.collection(u'barrels')
"""
if not barrels_ref.get():
    barrels_ref.document().set({
        'batch': '',
        'batchs_date: [],
        'condition': '',
        'conditions_history': [],
        'date': [],
        'gasified': '',
        'id': '',
        'liters': 0,
        'location': '',
        'serial': '',
        'style': '',
        'user':  [],
        'action': []
    })
"""
admin_ref = db.collection('admins')
"""
if not admin_ref.get():
    admin_ref.document().set({
        'nick': '',
        'password': '',
        'role': '',
        'user': ''
    })
"""
session_ref = db.collection('sessions')
"""
if not session_ref.get():
    session_ref.document().set({
        'last_login': '',
        'logged_in': '',
        'logout': '',
        'username': ''
    })
"""
client_ref = db.collection('clients')
"""
if not client_ref.get():
    client_ref.document().set({
        'address': '',
        'email': '',
        'id': '',
        'name': '',
        'phone': ''
    })
"""
liters_ref = db.collection('liters')
"""
if not liters_ref.get():
    liters_ref.document().set({
        'quantity': 0,
    })
"""
batchs_ref = db.collection(u'batchs')
"""
if not batchs_ref.get():
    batchs_ref.document().set({
        'batch': '',
    })
"""
styles_ref = db.collection(u'styles')
"""
if not styles_ref.get():
    styles_ref.document().set({
        'name': '',
    })
"""
contract_ref = db.collection(u'contracts')
"""
if not contract_ref.get():
    contract_ref.document().set({
        'barrel_id': '',
        'client': '',
        'client_id': '',
        'condition': '',
        'end_date': '',
        'init_date': '',
    })
"""



app.secret_key = b'\xb0\xf4u\xcc\\\xf5\x10\xd2\xcaO7\xcd\x0cXm\xd2'
app.config['SECRET_KEY'] = 'lagash-password'







def get_role_id(value):

    if value:
        aux = value.split("session__")

        if len(aux) > 1:
            role_id = aux[1][0]
            return role_id
        else:
            return None




# Función para calcular las fechas según la opción seleccionada
def calcular_fechas(opcion):
    fecha_actual = datetime.now()

    if opcion == '1-semana':
        fecha_inicio = fecha_actual - timedelta(days=7)
        fecha_fin = fecha_actual
    elif opcion == '2-semanas':
        fecha_inicio = fecha_actual - timedelta(days=14)
        fecha_fin = fecha_actual
    elif opcion == '3-semanas':
        fecha_inicio = fecha_actual - timedelta(days=21)
        fecha_fin = fecha_actual
    elif opcion == '4-semanas':
        fecha_inicio = fecha_actual - timedelta(days=28)
        fecha_fin = fecha_actual
    elif opcion == '1-mes':
        fecha_inicio = fecha_actual - timedelta(days=30)
        fecha_fin = fecha_actual
    elif opcion == 'rango-fechas':
        fecha_inicio_str = request.form['fecha-inicio']
        fecha_fin_str = request.form['fecha-fin']

        # Adjust the format to match the actual format of the date string
        fecha_inicio = datetime.strptime(fecha_inicio_str, '%d/%m/%Y')
        fecha_fin = datetime.strptime(fecha_fin_str, '%d/%m/%Y')

    # Formatear las fechas en el formato deseado (YYYY-MM-DD HH:MM)
    fecha_inicio = fecha_inicio.strftime('%Y-%m-%d %H:%M')
    fecha_fin = fecha_fin.strftime('%Y-%m-%d %H:%M')

    return fecha_inicio, fecha_fin




def buscar_barriles_en_rango(fecha_inicio, fecha_fin):

    # Convierte las fechas a objetos datetime si no lo están
    if not isinstance(fecha_inicio, datetime):
        fecha_inicio = datetime.strptime(fecha_inicio, '%Y-%m-%d %H:%M')
    if not isinstance(fecha_fin, datetime):
        fecha_fin = datetime.strptime(fecha_fin, '%Y-%m-%d %H:%M')
    
    barriles_encontrados = []
    barrels = functions_module.get_barrels(barrels_ref)

    for resultado in barrels:
        barril = resultado.to_dict()
        if 'date' in barril and isinstance(barril['date'], list):
            fechas_coincidentes = []
            conditions_history_coincidente = []
            batch_coincidentes = []


            for fecha_str in barril['date']:
                fecha = datetime.strptime(fecha_str.split()[0], '%d/%m/%Y')
                if fecha_inicio <= fecha <= fecha_fin:
                    fechas_coincidentes.append(fecha_str)
                    index_fecha = barril['date'].index(fecha_str)
                    conditions_history_coincidente.append(barril['conditions_history'][index_fecha])
                    #batch_coincidentes.append(barril['bat'])

            if fechas_coincidentes:
                # Actualiza el campo 'date' con las fechas coincidentes
                barril['date'] = fechas_coincidentes

                # Actualiza el campo 'conditions history' con los valores coincidentes
                barril['conditions_history'] = conditions_history_coincidente

                # Elimina claves no necesarias
                keys_a_eliminar = ['gasified', 'action', 'user']
                for key in keys_a_eliminar:
                    if key in barril:
                        del barril[key]
                        
                # Agrega el conteo de elementos en 'conditions_history'
                barril['conditions_history_count'] = len(conditions_history_coincidente)


                barriles_encontrados.append(barril)

    return barriles_encontrados



def get_admin_of_session(value):
    
    """
    Retorna una cadena de texto sin el prefijo 'session__' y 
    el primer caracter inmediatamente después de esta expresión.
    """
    if not isinstance(value, str) or not value.startswith('session__'):
        return None
    
    return value[len('session__'):][1:]




def get_user_info():
    cookie = functions_module.get_cookie(request)
    user_id = get_admin_of_session(cookie)

    user_dict = admin_ref.document(user_id).get().to_dict()

    return user_dict.get('user'), user_dict.get('nick')
    


#----------- RUTAS -----------

@app.route('/')
def index():

    return render_template('index.html')




@app.route('/home')
def general():
    title = 'GENERAL'
    username, nick = get_user_info()
    response1, response2, list_barrel, barrel_count, barrel_count_batch, barrel_location, location_condition = functions_module.process_barrels(barrels_ref)
    percent = functions_module.percentage(barrels_ref)
    style = functions_module.show_styles(styles_ref)
    contracts = functions_module.get_contracts(contract_ref)
    date = functions_module.get_time()


    cookie = functions_module.get_cookie(request)
    role = get_role_id(cookie)
  
    total_barrel = response1[5]
    barrels_in_table = 0
    if barrel_count and isinstance(barrel_count, dict):
        barrels_in_table = sum(info.get('count', 0) for type, info in barrel_count.items())

    warning = ""

    if total_barrel != barrels_in_table:
        warning = "*Advertencia: La cantidad de barriles útiles no coincide con la cantidad listada."
   
    contracts = functions_module.get_contracts(contract_ref)
    date = functions_module.get_time()
    
    dias_restantes = 10


    expired_contracts = [contract for contract in contracts if datetime.strptime(date, "%d/%m/%Y %H:%M") >= datetime.strptime(contract['end_date'], "%d/%m/%Y %H:%M") 
                         and contract['condition'] == 'Current']
    
    # Calcular la fecha límite
    end_date = functions_module.get_new_date(date, dias_restantes)

    # Convert 'end_date' to datetime
    end_date = datetime.strptime(end_date, "%d/%m/%Y %H:%M")


    # Filtrar contratos que cumplen con las condiciones
    to_expire_contracts = [
    contract for contract in contracts if datetime.strptime(contract['end_date'], "%d/%m/%Y %H:%M") <= end_date
    and contract['condition'] == 'Current'
]
    
    expire1 = bool(to_expire_contracts)
    expire2 = bool(expired_contracts)
    


    return render_template('general.html', contracts=contracts,location_condition = location_condition, 
                           barrel_location= barrel_location, date=date, total=response1, sl=response2, percent=percent, 
                           barrel_count = barrel_count, barrel_count_batch=barrel_count_batch, style = style, role=role, nick=nick, warning= warning, expire= expire1,expire2= expire2, title = title)



@app.route('/management')
def management():
    title = 'GESTIÓN'
    username, nick = get_user_info()
    response1, response2, list_barrel, barrel_count, barrel_count_batch, barrel_location, location_condition = functions_module.process_barrels(barrels_ref)
    quantity = functions_module.show_liters(liters_ref)
    style = functions_module.show_styles(styles_ref)
    batch = functions_module.show_batchs(batchs_ref)
    clients = functions_module.show_clients(client_ref)
    cookie = functions_module.get_cookie(request)
    role = get_role_id(cookie)

    return render_template('panel.html', quantity = quantity, batch = batch, style= style, list_barrel= list_barrel, 
                           role= role,clients=clients, nick=nick, title = title)



@firestore.transactional
def get_session_data(transaction, session_id, user):
    """ Looks up (or creates) the session with the given session_id.
        Creates a random session_id if none is provided. Increments
        the number of views in this session. Updates are done in a
        transaction to make sure no saved increments are overwritten.
    """
    date = functions_module.get_time()
    doc_ref = session_ref.document(document_id=session_id)
    doc = doc_ref.get(transaction=transaction)

    if doc.exists:
        session = doc.to_dict()
        # Mantener el valor actual del campo logout
        session['logout'] = session.get('logout', '')

        transaction.update(doc_ref, {
            'username': user,
            'logged_in': True,
            'last_login': date,
            'logout': session['logout']
        })
    else:
        session = {
            'username': user,
            'logged_in': True,
            'last_login': date,
            'logout': ''
        }   
        transaction.set(doc_ref, session)

    session['session_id'] = session_id
    return session





@app.route('/login', methods=['GET', 'POST'])
def login():
    if request.method == 'POST':
        user = request.form["user"]
        password = request.form["password"]
        
        admin = admin_ref.get()
        session = None
        
        for doc in admin:
            if format(doc.get('user')) == user and format(doc.get('password')) == password:
               
                session_id = f"session__{doc.get('role')}{doc.id}"
                transaction = db.transaction()
                session = get_session_data(transaction, session_id, user)
                resp = make_response(redirect('home'))
                resp.set_cookie('__session', session['session_id'], httponly=True, path='/')
                resp.set_cookie('access_token', httponly=True, path='/')
                     
                               
                return resp

        # Si se alcanza este punto, no se encontró ninguna coincidencia en el bucle for
        error_msg = 'No existe el usuario o la contraseña'
        return render_template('index.html', error_msg=error_msg)

    # Si la solicitud no es POST, devolver una respuesta de redireccionamiento
    return redirect('/')
            




@app.route('/logout', methods=['GET', 'POST'])
def logout():
    session_id = request.cookies.get('__session')
    date = functions_module.get_time()
    #token = request.cookies.get('access_token')
    if session_id:
        doc_ref = session_ref.document(document_id=session_id)
        doc = doc_ref.get()
        if doc.exists:
            session_data = doc.to_dict()
            session_data['logged_in'] = False
            session_data['logout'] = date
            doc_ref.update(session_data)
        resp = make_response(redirect('/'))
        #resp.delete_cookie('access_token')
        resp.delete_cookie('__session')
        return resp
    else:
        return redirect('/')




@app.route('/config')
def config():
    
    title = 'CONFIGURACIÓN'
    username, nick = get_user_info()
    cookie = functions_module.get_cookie(request)
    role = get_role_id(cookie)
    admins = functions_module.get_admins(admin_ref)
    sessions = functions_module.get_sessions(session_ref)
 
    return render_template('/config.html', role = role, admins = admins, sessions = sessions, nick=nick, title=title)



@app.route('/clients')
def clients():
    
    title = 'CLIENTES'
    username, nick = get_user_info()
    clients = functions_module.get_clients(client_ref)
    cookie = functions_module.get_cookie(request)
    role = get_role_id(cookie)
    
    
    return render_template('/client.html', role = role, clients = clients, nick=nick,title=title)



@app.route('/barrel_condition')
def barrel_condition():

    title = 'CAMBIAR ESTADO DEL BARRIL'
    username, nick = get_user_info()
    response1, response2, list_barrel, barrel_count, barrel_count_batch, barrel_location, location_condition = functions_module.process_barrels(barrels_ref)
    cookie = functions_module.get_cookie(request)
    role = get_role_id(cookie)
    batch = functions_module.show_batchs(batchs_ref)
    
    return render_template('/barrel_condition.html', role = role, list_barrel = list_barrel, nick=nick, batch=batch, title=title)



@app.route('/contracts')
def contracts():

    title = 'CONTRATOS'
    username, nick = get_user_info()
    cookie = functions_module.get_cookie(request)
    role = get_role_id(cookie)
    contracts = functions_module.get_contracts(contract_ref)
    date = functions_module.get_time()
    
    expired_contracts = [contract for contract in contracts if datetime.strptime(date, "%d/%m/%Y %H:%M") >= datetime.strptime(contract['end_date'], "%d/%m/%Y %H:%M") 
                         and contract['condition'] == 'Current']

    response = bool(expired_contracts)

    return render_template('/contracts.html', role = role, title=title, contracts= contracts, date=date, response = response, nick= nick)



@app.route('/record', methods=['GET', 'POST'])
def record():

    title = 'HISTORIAL'

    percent = functions_module.percentage(barrels_ref)
    style = functions_module.show_styles(styles_ref)
    username, nick = get_user_info()
    cookie = functions_module.get_cookie(request)
    role = get_role_id(cookie)
    fechas = None
    estado_contador = None
    img_base64 = None
    not_fechas = 'No se seleccionó nada aún'
    range_date= None

    if request.method == 'POST':

        opcion = request.form['opcion']
        fecha_inicio, fecha_fin = calcular_fechas(opcion)
        fechas = buscar_barriles_en_rango(fecha_inicio, fecha_fin)
        print(fecha_inicio, fecha_fin)
        print(fechas)

        # Inicializamos un diccionario para contar los estados
        estado_contador = {'Sucio': 0, 'Vacío': 0, 'En servicio': 0, 'En almacenamiento': 0}

        for item in fechas:
            if 'conditions_history' in item:
                for estado in item['conditions_history']:
                    # Incrementamos el contador del estado correspondiente
                    estado_contador[estado] += 1
        
        if not fechas:
            not_fechas= 'No hay nada para mostrar'

        if fechas and estado_contador is not None:
            not_fechas = None
            img_base64 = functions_module.draw_bar_graphics(estado_contador)

        
        if fecha_inicio and fecha_fin:
            init_date = datetime.strptime(fecha_inicio, '%Y-%m-%d %H:%M').strftime('%d-%m-%Y %H:%M')
            end_date = datetime.strptime(fecha_fin, '%Y-%m-%d %H:%M').strftime('%d-%m-%Y %H:%M')
            range_date = f'desde {init_date} hasta {end_date}'

    return render_template('/record.html', role = role, title=title, style= style,
                           nick= nick, fechas=fechas, range_date = range_date, not_fechas = not_fechas ,img_base64=img_base64 )


@app.route('/options/<id>')
def options(id):

    title = 'OPCIONES'
    response1, response2, list_barrel, barrel_count, barrel_count_batch, barrel_location, location_condition = functions_module.process_barrels(barrels_ref)

   
    barrel_data = functions_module.get_data_by_id(id, barrels_ref)
    functions_module.verify_cycle_counter(id, barrels_ref, barrel_data)
    quantity = functions_module.show_liters(liters_ref)
    style = functions_module.show_styles(styles_ref)
    batch = functions_module.show_batchs(batchs_ref)
    clients = functions_module.show_clients(client_ref)
    username, nick = get_user_info()
    cookie = functions_module.get_cookie(request)
    role = get_role_id(cookie)
 
    average = functions_module.average_time_state(barrel_data)
    cycle_count = functions_module.cycle_counter(barrel_data)


  
    return render_template('/options.html', role = role, title=title, id = id, quantity=quantity, style=style, batch=batch, clients=clients, 
                           barrel_data=barrel_data, cycle_count = cycle_count, average = average, nick= nick)






# Un "middleware" que se ejecuta antes de responder a cualquier ruta. Aquí verificamos si el usuario ha iniciado sesión
@app.before_request
def antes_de_cada_peticion():
    ruta = request.path
    session = functions_module.get_cookie(request)
    
    """
    # Excluir rutas de verificación de token JWT
    rutas_excluidas = ["/", "/login", "/logout", "/static"]

    if ruta not in rutas_excluidas:
        token = request.cookies.get('access_token')

        if not token:
            print('entra')
            return redirect("/")

        username = verify_jwt(token)

        if not username:
            # El token JWT es inválido o ha expirado, maneja el caso según tus necesidades
            return redirect("/logout")
    """

    # Verificamos si existe una cookie con el nombre de usuario
    if not session and ruta != "/" and ruta != "/login" and ruta != "/logout" and not ruta.startswith("/static"):
        return redirect("/")
    # Si ya ha iniciado, no hacemos nada, es decir lo dejamos pasar
    if session:
        if ruta == "/" or ruta == "/login":
            return redirect('/home')
        
        role = get_role_id(session)
        if role != '1' and ruta == '/config':
            return redirect('/home')





@app.route('/add_client', methods=['GET', 'POST'])
def add_client():

    if request.method == 'POST':
        name = request.form['client-name']
        id = request.form['id']
        phone = request.form['phone']
        email = request.form['email']
        address = request.form['address']

        existing_client = client_ref.where('phone', '==', phone).get()
        existing_client_by_id = client_ref.where('id', '==', id).get()
        existing_client_by_email = client_ref.where('email', '==', email).get()

        if existing_client or existing_client_by_id or existing_client_by_email:
            session['error_msg'] ='El cliente ya existe.'
        else:
            client_ref.add({'name': name,'id': id, 'phone':phone, 'email':email, 'address': address})
            session['success_msg'] = 'Cliente agregado correctamente.'
    return redirect('/clients')




@app.route('/remove_client/<client>', methods=['GET', 'POST'])
def remove_client(client):

    client_href = client_ref.where('id', '==', client).get()
    contract_href = contract_ref.where('client_id', '==', client).get()

    if contract_href:
        contract = contract_href[0].to_dict()
        condition = contract.get('condition') 
        if condition == 'Ended':
            for client in client_href:
                client.reference.delete()
            session['success_msg'] = 'El cliente ha sido eliminado correctamente.'
        elif condition == 'Canceled':
            session['error_msg'] = 'El cliente tiene un contrato en el área de verificación, finalíce el contrato primero para poder eliminar.'

        else:
            session['error_msg'] = 'El cliente tiene un contrato en vigencia, no se puede eliminar.'
    else:
        for client in client_href:
            client.reference.delete()
        session['success_msg'] = 'El cliente ha sido eliminado correctamente.'
    
    return redirect('/clients')




@app.route('/update_client', methods=['POST', 'GET'])
def update_client():

    if request.method != 'POST':
        return redirect('/clients')
   
    name = request.form['name']
    hname = request.form['hname']
    id = request.form['id']
    number = request.form['number']
    phone = request.form['phone']
    hphone = request.form['hphone']
    email = request.form['email']
    hemail = request.form['hemail']
    address = request.form['address']
    haddress = request.form['haddress']
   

    data = None
    if number:
        data = client_ref.where('id', '==', number).get()
    if phone:
        data = client_ref.where('phone', '==', phone).get()
    if email:
        data = client_ref.where('email', '==', email).get()
        
    if data:
        #data = docs[0].to_dict()
        session['error_msg'] = 'Ya existe un cliente con esos datos.'
        return redirect('/clients')
    
    else:
        data = client_ref.where('id', '==', id).get()
            
        
    num = str(number) if number else str(id)
    p = str(phone) if phone else str(hphone)
    e = str(email) if email else str(hemail)
    n = str(name) if name else str(hname)
    a = str(address) if address else str(haddress)

    if data:
        for doc in data:
            doc_id = str(doc.id)
        
        # Updating the data in the database 
            client_ref.document(doc_id).update({
                'id': num,
                'email': e,
                'address': a,
                'name': n,
                'phone': p
            
            })
        session['success_msg'] ='Se actualizó con éxito.'

    
    return redirect('/clients')



@app.route('/add_user', methods=['GET','POST'])
def add_user():

    if request.method == 'POST':
        username = request.form['username']
        nickname = request.form['nick']
        password = request.form['password']
        role = request.form['role']
        if role == 'Administrador':
            role = '1'
        else:
            role = '6'

        existing_admin = admin_ref.where('user', '==', username).get()

        if existing_admin:
            session['error_msg'] ='El usuario ya existe.'
        
        else:        
            admin_ref.add({'user': username, 'nick': nickname, 'password':password, 'role':role})
            session['success_msg'] = 'Usuario agregado correctamente.'
    return redirect('/config')




@app.route('/remove_user/<admin>', methods=['GET','POST'])
def remove_user(admin):

    href = admin_ref.where('user', '==', admin).get()
    session_href = session_ref.where('username', '==', admin).get()
    
    for admin in href:
        if (admin.to_dict()['role'] != '1'):
            admin.reference.delete()
            for admin_session in session_href:
                admin_session.reference.delete()
            session['success_msg'] = 'Usuario eliminado correctamente.'
    return redirect('/config')




#TODO agregar contador ciclo de vida barril

@app.route('/add_barrel', methods=['GET', 'POST'])
def add_barrel():  
    
    if request.method == 'POST':
        id = request.form['number']
        serial = request.form['serial-number']
        batch = request.form['batch']
        liters = request.form['liters']
        condition = request.form['condition']   
        location = request.form['location']
        gasified = 'SI' if request.form.get('check-gas') else 'NO'

        style = functions_module.get_style_of_batch(batch)

       
        date = functions_module.get_time()
      
        existing_barrel = barrels_ref.where('id', '==', id).get()
        existing_serial = barrels_ref.where('serial', '==', serial).get()

        if existing_barrel:
           
            return jsonify({"success": False, "message": "El barril ya existe."})

        if  existing_serial:
           
            return jsonify({"success": False, "message": "El número serial ya existe."})

        
        if condition == 'Vacío' or condition == 'Sucio':
            batch = ''
            style = ''
        
        username, nickname = get_user_info()
        user = ",".join([username,nickname])
        date = functions_module.get_time()
        action = 'Nuevo Registro'
        barrel_data = {
                "id":id, 
                "serial":serial,
                "batch":batch, 
                "batchs_history": [batch],
                "batchs_date":[date],
                "liters":int(liters), 
                "condition": condition,
                "style": style, 
                "location": location, 
                "gasified": gasified,     
                "conditions_history": [condition],
                "date": [date],
                "user": [user],
                "action": [action],
                
     
            }
        barrels_ref.add(barrel_data)
     
        return jsonify({"success": True, "message": "Barril agregado con exito."})

   



@app.route('/delete/<id>', methods=['POST', 'GET'])
def delete(id):

    href = barrels_ref.where('id', '==', id).get()
    contract_href = contract_ref.where('barrel_id', '==', id).get()
    
    if contract_href:
        contract = contract_href[0].to_dict()
        condition= contract.get('condition') 
        if condition == 'Ended':
            for barrel in href:
                barrel.reference.delete()
            session['success_msg'] ='Barril eliminado con éxito.'
        elif condition == 'Canceled':
            session['error_msg'] = 'El barril está en el área de verificación, finalíce el contrato primero para poder eliminar.'
        else:
            session['error_msg'] = 'El barril está alquilado, no se puede eliminar.'
    else:
        for barrel in href:
            barrel.reference.delete()
        session['success_msg'] = 'Barril eliminado con éxito.'
  
    
    return redirect('/management')





@app.route('/change', methods=['POST', 'GET'])
def change():
    
    if request.method != 'POST':
        return redirect('/barrel_condition')
    
    
    barrel_ids = [y for y in (x.strip() for x in request.form['ids'].split(',')) if y]
    new_condition = request.form.get('condition')

    
    for barrel_id in barrel_ids:
        
        doc, doc_ref = functions_module.barrel_exist(barrel_id, barrels_ref)    
       
    if doc:
        old = doc.to_dict()['condition']
        new_date = functions_module.get_time()
        user, nickname = get_user_info()
        new_user = ",".join([user, nickname])
        new_action = f'Cambia de {old} -> {new_condition}'

        if new_condition in ['Sucio', 'Vacío', 'En almacenamiento', 'En servicio']:
        
            if 'conditions_history' in doc.to_dict() and 'date' in doc.to_dict() and 'user' in doc.to_dict():
                conditions_history = doc.to_dict()['conditions_history']
                conditions_history.append(new_condition)
                date = doc.to_dict()['date']
                date.append(new_date)
                user = doc.to_dict()['user']
                user.append(new_user)
                action = doc.to_dict()['action']
                action.append(new_action)
              

                change_cycle = doc.to_dict().get('change_cycle', 0) + 1  # incrementa el contador de ciclos de cambio
                doc_ref.update({'conditions_history': conditions_history, 'date': date, 'condition': new_condition, 'user': user, 
                                'action': action, 'change_cycle': change_cycle})
            else:
                doc_ref.update({'conditions_history': [new_condition], 'date': [new_date], 'condition': new_condition, 
                                'user': [new_user], 'action': [new_action], 'change_cycle': 1})  # establece el contador de ciclos de cambio en 1 si es el primer cambio de estado

            if new_condition == 'Sucio' or new_condition == 'Vacío':
                doc_ref.update({'batch': '', 'style': '', 'gasified':'NO'})

        elif new_condition in ['Bar', 'Fábrica']:
            doc_ref.update({'location': new_condition})

        else:
            if 'batchs_history' in doc.to_dict():
                batchs_history = doc.to_dict()['batchs_history']
                batchs_history.append(new_condition)
                batchs_date = doc.to_dict()['batchs_date']
                batchs_date.append(new_date)
                style = functions_module.get_style_of_batch(new_condition)
                doc_ref.update({'batch': new_condition, 'style': style, 'batchs_history': batchs_history, 'batchs_date': batchs_date})
            else:
                doc_ref.update({'batchs_history': [new_condition], 'batchs_date': [new_date]})
            

        session['success_msg'] ='El cambio de estado se realizó con éxito.'

    return redirect('/barrel_condition')





"""
Revisar funcion update ***

"""
@app.route('/update', methods=['POST', 'GET'])
def update():

    if request.method != 'POST':
        return redirect('/management')
   

    id = request.form['id']
    serial = request.form['serial']
    batch = request.form['batchs']
    number = request.form['number']
    serial_number = request.form['number-serial']
    liters = request.form['liters']
    condition = request.form['condition']
    style = functions_module.get_style_of_batch(batch)
    location = request.form['location']
    gasified = request.form['gasified']
    username, nickname = get_user_info()

    if number:
        if not functions_module.validate_field(id):
            session['error_msg'] ='Sólo se permiten números, letras y guiones.'
            return redirect(f'/options/{id}')
   
    if serial:
        if not functions_module.validate_field(id):
            session['error_msg'] ='Sólo se permiten números, letras y guiones.'
            return redirect(f'/options/{id}')
 
    if not all([liters, condition, location]):
        session['error_msg'] ='Por favor, complete todos los campos.'
        return redirect(f'/options/{id}')

   
    if condition == 'Sucio' or condition == 'Vacío':
        batch = ''

    data = None
    if number:      
        data = barrels_ref.where('id', '==', number).get()
    if serial_number:
        data = barrels_ref.where('serial', '==', serial_number).get()
        
    if data:
        session['error_msg'] ='El barril ya existe.'
        return redirect(f'/options/{id}')
    
    else:
        data = barrels_ref.where('id', '==', id).get()
        
        conditions_history = data[0].to_dict()['conditions_history']
        new_date = data[0].to_dict()['date']
        new_user = data[0].to_dict()['user']
        new_action = data[0].to_dict()['action']
        batch_history = data[0].to_dict()['batchs_history']
        batchs_date = data[0].to_dict()['batchs_date']

        if conditions_history and new_date and new_user and new_action:
            conditions_history[-1] = condition
            new_date[-1] = functions_module.get_time()
            new_user[-1] = ",".join([username, nickname])
            new_action[-1] = 'Actualización de registro'
            batch_history[-1] = batch 
            batchs_date[-1] = functions_module.get_time()

    n = str(number) if number else str(id)
    s = str(serial_number) if serial_number else str(serial)

    for doc in data:
        doc_id = (u'{}'.format(doc.id))
       
    # Updating the data in the database 
        barrels_ref.document(doc_id).update({
            'id': n,
            'serial': s,
            'batch': batch,
            'liters': int(liters),
            'condition': condition,
            'style': style,
            'location': location,
            'gasified': gasified,
            'batchs_history': batch_history,
            'batchs_date': batchs_date,
            'conditions_history': conditions_history,
            'date': new_date,
            'user': new_user, 
            'action' : new_action,
        })
    session['success_msg'] = 'Se actualizó con éxito.'

    
    return redirect(f'/options/{n}')

    



#TODO aumentar contador
@app.route('/add_contract', methods=['GET', 'POST'])
def add_contract():
   
    if request.method != 'POST':
        return redirect('/contracts')
   
   
    init_date = functions_module.get_time()
    days= request.form['days']
    client = request.form['contract-client']
    id = request.form['hclient-id']
    barrel_id = request.form['barrel']

    existing_contracts = contract_ref.where('barrel_id', '==', barrel_id).get()
    for contract in existing_contracts:
        contract_data = contract.to_dict()
        if contract_data.get('condition') == 'Current':   
            session['error_msg'] ='El barril ya se encuentra alquilado.'
    else:
        doc, doc_ref= functions_module.barrel_exist(barrel_id, barrels_ref)
        if doc:
            new_date = functions_module.get_time()
            user, nickname = get_user_info()
            new_user = ",".join([user, nickname])
            new_action = f'Alquilado'
            end_date = functions_module.get_new_date(init_date, days)
            contract_ref.add({'init_date': init_date, 'end_date': end_date, 'client': client,'client_id': id,'barrel_id': barrel_id, 'condition': 'Current'})
            functions_module.quick_update(barrel_id, client, barrels_ref)
            if 'conditions_history' in doc.to_dict() and 'date' in doc.to_dict() and 'user' in doc.to_dict():
                conditions_history = doc.to_dict()['conditions_history']
                conditions_history.append('En servicio')
                date = doc.to_dict()['date']
                date.append(new_date)
                user = doc.to_dict()['user']
                user.append(new_user)
                action = doc.to_dict()['action']
                action.append(new_action)
                doc_ref.update({'conditions_history': conditions_history, 'date': date, 'condition': 'En servicio', 'user': user, 'action': action})
            else:               
                doc_ref.update({'conditions_history': ['En servicio'], 'date': [new_date], 'condition': 'En servicio', 'user': [new_user], 'action': [new_action]})
        session['success_msg'] = 'El contrato se ha realizado correctamente.'

    return redirect('/contracts')





@app.route('/remove_contract/<id>', methods=['GET', 'POST'])
def remove_contract(id):
   
    href = contract_ref.where('barrel_id', '==', id).get()
    doc, doc_ref= functions_module.barrel_exist(id, barrels_ref)
    if doc:
        new_date = functions_module.get_time()
        user, nickname = get_user_info()
        new_user = ",".join([user, nickname])
        new_action = f'Alquiler Eliminado'
        
       
        if 'conditions_history' in doc.to_dict() and 'date' in doc.to_dict() and 'user' in doc.to_dict():
            conditions_history = doc.to_dict()['conditions_history']
            conditions_history.append('En Almacenamiento')
            date = doc.to_dict()['date']
            date.append(new_date)
            user = doc.to_dict()['user']
            user.append(new_user)
            action = doc.to_dict()['action']
            action.append(new_action)
            doc_ref.update({'conditions_history': conditions_history, 'date': date, 'condition': 'En almacenamiento', 'user': user, 'action': action})

        else:      
        
            doc_ref.update({'conditions_history': ['En almacenamiento'], 'date': [new_date], 'condition': 'En almacenamiento', 'user': [new_user], 'action': [new_action]})


    for contract in href:
      
        contract.reference.delete()
        functions_module.quick_update(id, 'Fábrica', barrels_ref)
        session['success_msg'] ='Contrato eliminado con éxito.'
  
    return redirect('/contracts')




@app.route('/erase/<id>', methods=['GET', 'POST'])
def erase_contract(id):
   
    href = contract_ref.where('barrel_id', '==', id).get()
    doc, doc_ref= functions_module.barrel_exist(id, barrels_ref)
    if doc:
        new_date = functions_module.get_time()
        user, nickname = get_user_info()
        new_user = ",".join([user, nickname])
        new_action = f'Alquiler Terminado/Cancelado'
        
       
        if 'conditions_history' in doc.to_dict() and 'date' in doc.to_dict() and 'user' in doc.to_dict():
            conditions_history = doc.to_dict()['conditions_history']
            conditions_history.append('Sucio')
            date = doc.to_dict()['date']
            date.append(new_date)
            user = doc.to_dict()['user']
            user.append(new_user)
            action = doc.to_dict()['action']
            action.append(new_action)
            doc_ref.update({'conditions_history': conditions_history, 'date': date, 'condition': 'Sucio', 'user': user, 'action': action, 'batch': '', 'style': ''})

        else:      
        
            doc_ref.update({'conditions_history': ['Sucio'], 'date': [new_date], 'condition': 'Sucio', 'user': [new_user], 'action': [new_action], 'batch': '', 'style': ''})


    for contract in href:
      
        contract.reference.delete()
        functions_module.quick_update(id, 'Fábrica', barrels_ref)
        session['success_msg'] ='Contrato cancelado/eliminado con éxito.'
  
    return redirect('/contracts')





#TODO aumentar contador
@app.route('/end_contract/<id>', methods=['GET', 'POST'])
def end_contract(id):
   
    href = contract_ref.where('barrel_id', '==', id).get()


    doc, doc_ref= functions_module.barrel_exist(id, barrels_ref)
    if doc:
        new_date = functions_module.get_time()
        user, nickname = get_user_info()
        new_user = ",".join([user, nickname])
        new_action = f'Alquiler Finalizado'
        
        functions_module.quick_update(id, 'Fábrica', barrels_ref)
        if 'conditions_history' in doc.to_dict() and 'date' in doc.to_dict() and 'user' in doc.to_dict():
            conditions_history = doc.to_dict()['conditions_history']
            conditions_history.append('Sucio')
            date = doc.to_dict()['date']
            date.append(new_date)
            user = doc.to_dict()['user']
            user.append(new_user)
            action = doc.to_dict()['action']
            action.append(new_action)
            doc_ref.update({'conditions_history': conditions_history, 'date': date, 'condition': 'Sucio', 'user': user, 'action': action, 'batch': '', 'style': ''})

        else:      
        
            doc_ref.update({'conditions_history': ['Sucio'], 'date': [new_date], 'condition': 'Sucio', 'user': [new_user], 'action': [new_action], 'batch': '', 'style': ''})

    for doc in href:
        doc_id = (u'{}'.format(doc.id))
       
    # Updating the data in the database 
        contract_ref.document(doc_id).update({
            'condition': 'Ended'
        })
        
        session['success_msg'] ='Contrato finalizado con éxito.'
  
    return redirect('/contracts')






@app.route('/cancel_contract/<id>', methods=['GET', 'POST'])
def cancel_contract(id):
   
    href = contract_ref.where('barrel_id', '==', id).get()
    doc, doc_ref= functions_module.barrel_exist(id, barrels_ref)
    if doc:
        new_date = functions_module.get_time()
        user, nickname = get_user_info()
        new_user = ",".join([user, nickname])
        new_action = f'Alquiler Cancelado'
        
       
        if 'conditions_history' in doc.to_dict() and 'date' in doc.to_dict() and 'user' in doc.to_dict():
            conditions_history = doc.to_dict()['conditions_history']
            conditions_history.append('En Servicio')
            date = doc.to_dict()['date']
            date.append(new_date)
            user = doc.to_dict()['user']
            user.append(new_user)
            action = doc.to_dict()['action']
            action.append(new_action)
            doc_ref.update({'conditions_history': conditions_history, 'date': date, 'condition': 'En servicio', 'user': user, 'action': action})

        else:      
        
            doc_ref.update({'conditions_history': ['En servicio'], 'date': [new_date], 'condition': 'En servicio', 'user': [new_user], 'action': [new_action]})


    for doc in href:
        doc_id = (u'{}'.format(doc.id))
       
    # Updating the data in the database 
        contract_ref.document(doc_id).update({
            'condition': 'Canceled'
        })
        
        session['success_msg'] ='Contrato cancelado correctamente.'

    return redirect('/contracts')






@app.route('/resume_contract/<id>', methods=['GET', 'POST'])
def resume_contract(id):
   

    href = contract_ref.where('barrel_id', '==', id).get()

    doc, doc_ref= functions_module.barrel_exist(id, barrels_ref)
    if doc:
        new_date = functions_module.get_time()
        user, nickname = get_user_info()
        new_user = ",".join([user, nickname])
        new_action = f'Alquiler Reanudado'
        
       
        if 'conditions_history' in doc.to_dict() and 'date' in doc.to_dict() and 'user' in doc.to_dict():
            conditions_history = doc.to_dict()['conditions_history']
            conditions_history.append('En Servicio')
            date = doc.to_dict()['date']
            date.append(new_date)
            user = doc.to_dict()['user']
            user.append(new_user)
            action = doc.to_dict()['action']
            action.append(new_action)
            doc_ref.update({'conditions_history': conditions_history, 'date': date, 'condition': 'En servicio', 'user': user, 'action': action})

        else:      
        
            doc_ref.update({'conditions_history': ['En servicio'], 'date': [new_date], 'condition': 'En servicio', 'user': [new_user], 'action': [new_action]})

    for doc in href:
        doc_id = (u'{}'.format(doc.id))
       
    # Updating the data in the database 
        contract_ref.document(doc_id).update({
            'condition': 'Current'
        })
        
        session['success_msg'] ='Contrato reanudado correctamente.'

    return redirect('/contracts')





"""
@app.route('/add_style', methods=['GET', 'POST'])
def add_style():
    exists = False
  
    if request.method == 'POST':
        style = request.form['style']

        doc = styles_ref
        styles = doc.get()

        for s in styles:
            if (format(s.get('name')) == style):      
                exists = True
                session['error_msg'] = 'El estilo ya existe.'
        if not exists:
            doc.add({"name": style})
            session['success_msg'] = 'Estilo agregado correctamente.'
        return redirect('/management')
"""



"""
@app.route('/remove/<style>', methods=['POST', 'GET'])
def remove_style(style):
   
    styles = styles_ref
    href = styles.where('name', '==', style).get()

    for style in href:

        style.reference.delete()
   
        session['success_msg'] ='Estilo eliminado con exito.'
    return redirect('/management')
"""




@app.route('/add_liters', methods=['GET', 'POST'])
def add_liters():
    exists = False

    if request.method == 'POST':
        liter = request.form['liters']
        
        if not liter.isdigit():
            return jsonify({"success": False, "message": "La cantidad de litros debe ser un número entero positivo."})

            
        elif int(liter) <= 0:
            return jsonify({"success": False, "message": "La cantidad de litros debe ser mayor que cero."})
        elif int(liter) >= 1000:
            return jsonify({"success": False, "message": "La cantidad de litros es EXAGERADAMENTE grande."})
        else:
            doc = liters_ref
            liters_query = doc.where('quantity', '==', int(liter)).get()
            
            if liters_query:
                return jsonify({"success": False, "message": "Hubo un problema al agregar los litros. Verifique que no se encuentre agregada dicha cantidad."})

            else:
                doc.add({"quantity": int(liter)})
              
                return jsonify({"success": True, "message": "Cantidad de litros agregada correctamente. Actualice la página para ver los cambios."})

    



@app.route('/removes/<liter>', methods=['POST', 'GET'])
def remove_liter(liter):

    liters = liters_ref
    
    href = liters.where(u'quantity', u'==', int(liter)).get()
    
    for liter in href:
        n = (u'{}'.format(liter.id))
        liters.document(n).delete()
      
        session['success_msg'] = 'Cantidad eliminada con éxito.'
    return redirect('/management')




@app.route('/add_batch_style', methods=['POST', 'GET'])
def add_batch():
    if request.method == 'POST':
        batch = request.form['batch']
        style = request.form['style']
        batch_style = f"{batch}, {style}"

        doc = batchs_ref
        batchs = doc.get()

        if batch_style in [format(b.get('batch')) for b in batchs]:
           
            return jsonify({"success": False, "message": "Hubo un problema al agregar el lote/estilo."})

        else:
            doc.add({"batch": batch_style})
            

            return jsonify({"success": True, "message": "Lote y estilo agregados correctamente. Actualice la página pra ver los cambios."})

   




@app.route('/del_batch/<batch>', methods=['POST', 'GET'])
def delete_batch(batch):

    batchs = batchs_ref

    href = batchs.where('batch' , '==', batch).get()

    for batch in href:
        batch.reference.delete()
        session['success_msg'] = 'Lote eliminado con éxito.'

    return redirect('/management')
     




#TODO faltan verificaciones de id y serial
@app.route('/load_excel', methods=['GET', 'POST'])
def load_excel():
    if request.method == 'POST':
        archivo = request.files['archivo_excel']
        existing_list = []
        existing_quantity = []
        if archivo:
            # Lee el archivo Excel utilizando pandas
            df = pd.read_excel(archivo, skiprows=1, usecols='A:H')
           
            # Asegúrate de que el archivo tenga las columnas esperadas
            columnas_esperadas = ['Nº', 'Serie', 'Estado', 'Ubicación', 'Estilo', 'Litros', 'Gasificado', 'Lote']
            if set(columnas_esperadas).issubset(df.columns):

                # Antes del bucle, obtener todos los barriles y litros existentes de la base de datos
                existing_barrels = {barrel.id: barrel for barrel in barrels_ref.get()}
                existing_serials = {barrel.serial: barrel for barrel in barrels_ref.get()}
                existing_liters = {lit.quantity for lit in liters_ref.get()}


                # Procesa los datos como desees, por ejemplo, puedes iterar a través de las filas y guardarlos en Firebase
                for _, fila in df.iterrows():
                    numero = fila['Nº']
                    serie = fila['Serie']
                    estado = fila['Estado']
                    ubicacion = fila['Ubicación']
                    estilo = fila['Estilo']
                    litros = fila['Litros']
                    gasificado = fila['Gasificado']
                    lote = fila['Lote']

                    # Guarda los datos en Firebase como desees
                      
                    username, nickname = get_user_info()
                    user = ",".join([username,nickname])
                    date = functions_module.get_time()
                    action = 'Nuevo Registro'
                    style = functions_module.get_style_of_batch(lote)

                 
                    existing = False
                   
                    if estado == 'Vacío' or estado == 'Sucio':
                        lote = ''
                        style = ''

                    patron = re.compile(r'^[sS][iI]?$')
                    patronB = re.compile(r'^[bB][aA]?[rR]?$')
                    patronF = re.compile(r'^[fF][aA]?[bB]?[rR]?[iI]?[cC]?[aA]?$')

                    if patron.match(gasificado):
                        gasificado = 'SI'
                    else:
                        gasificado = 'NO'

                    if patronB.match(ubicacion):
                        ubicacion = 'Bar'
                    elif patronF.match(ubicacion):
                        ubicacion = 'Fábrica'
                    
                    if numero in existing_barrels or serie in existing_serials:
                        existing = True
                        existing_list.append(numero)
                        continue

                    if litros not in existing_liters:
                        existing_quantity.append(numero)
                        continue
                        

                   
                    else:
                        barrel_data = {
                                "id":str(numero), 
                                "serial":str(serie),
                                "batch":lote,
                                "batchs_history": [lote],
                                "batchs_date":[date], 
                                "liters":int(litros), 
                                "condition": estado,
                                "style": style, 
                                "location": ubicacion, 
                                "gasified": gasificado,     
                                "conditions_history": [estado],
                                "date": [date],
                                "user": [user],
                                "action": [action],
                                                  
                            }
                        barrels_ref.add(barrel_data)
                
                if existing:
                    session['error_msg'] = "Revise los siguientes barriles, se encuentran repetidos %s." % existing_list
                elif existing_liters:
                    session['error_msg']= "Hay barriles con capacidades inexistentes %s." % existing_quantity
                else:
                    session['success_msg'] = "Datos del archivo Excel procesados y guardados."
            else:
                session['error_msg'] = "El archivo Excel no tiene las columnas esperadas."

    return redirect('/management')




@app.route('/clear/<id>', methods=['GET', 'POST'])
def clear(id):

    barrel = barrels_ref
    href = barrel.where('id' , '==', id).get()

    new_date = functions_module.get_time()
    user, nickname = get_user_info()
    new_user = ",".join([user, nickname])
    new_action = f'Registro Limpio'
    conditions = href[0].to_dict()['conditions_history'][-1]

    for doc in href:
        doc_id = (u'{}'.format(doc.id))
       
    # Updating the data in the database 
        barrels_ref.document(doc_id).update({
            
            'conditions_history': [conditions],
            'date': [new_date],
            'user': [new_user], 
            'action' : [new_action],
        })

    return redirect(f'/options/{id}')




@app.route('/clear_contract', methods=['GET', 'POST'])
def clear_contract():

    href = contract_ref.get()
    if not href:
        session['error_msg'] ='La tabla ya se encuentra vacía.'
    else:
        for contract in href:
            
            condition = contract.get('condition')
            if condition == 'Ended':
                contract.reference.delete()
                
                session['success_msg'] ='Se limpio exitosamente la tabla.'                       

    return redirect(f'/contracts')







if __name__ == "__main__":
    app.run()