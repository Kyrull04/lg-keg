from flask import session
import datetime as dt
from datetime import timedelta
import ntplib
import matplotlib.pyplot as plt
from io import BytesIO
import base64




def get_admins(admin_ref):
    try:
        admins = admin_ref.get()
        list_admin = [{'user': admin.get('user'), 
                    'nick': admin.get('nick'), 
                    'role': admin.get('role')
                   }
                for admin in admins]
        return list(list_admin)
    except Exception as e:
        print(f'Error al acceder a la colección admins: {e}')
        return []
    


def get_clients(client_ref):
    try:
        clients = client_ref.get()
        list_client = [{'name': client.get('name'), 
                    'id': client.get('id'), 
                    'phone': client.get('phone'),
                    'email': client.get('email'),
                    'address': client.get('address')
                   }
                for client in clients]
        return list(list_client)
    except Exception as e:
        print(f'Error al acceder a la colección clients: {e}')
        return []


def get_sessions(session_ref):
    try:
        sessions = session_ref.get()
        list_sessions = [{'last_login': session.get('last_login'), 
                    'logged_in': session.get('logged_in'), 
                    'logout': session.get('logout'),
                    'username': session.get('username'),
                   }
                for session in sessions]
        return list(list_sessions)
    except Exception as e:
        print(f'Error al acceder a la colección session: {e}')
        return []


def get_barrels(barrels_ref):
    try:
        barrels = barrels_ref.get()
        return list(barrels)
    except Exception as e:
        print(f'Error al acceder a la colección barrels: {e}')
        return []
    
    
def get_contracts(contracts_ref):
    try:
        contracts = contracts_ref.get()
        list_contract = [{'barrel_id': contract.get('barrel_id'), 
                    'client': contract.get('client'), 
                    'client_id': contract.get('client_id'),
                    'end_date': contract.get('end_date'),
                    'init_date': contract.get('init_date'),
                    'condition': contract.get('condition')
                    }
                for contract in contracts]
        return list(list_contract)
    except Exception as e:
        print(f'Error al acceder a la colección clients: {e}')
        return []



def get_cookie(request):
    session =  request.cookies.get('__session')
    return session


def show_clients(client_ref):
    clients = client_ref.get()
    return [{'name': client.get('name'), 'id': client.get('id')} if client.get('name') else {'name': ''} for client in clients]


def show_liters(l):
    liters = l.get()
    return [{'quantity':liter.get('quantity')} if liter.get('quantity') else {'quantity': ''} for liter in liters]
    

def show_styles(s):
   
    styles = s.get()
    return [{'name': style.get('name')}  if style.get('name') else {'name': ''} for style in styles]

def show_batchs(b):
    
    batchs = b.get()
    return [{'batch': batch.get('batch')} if batch.get('batch') else {'batch':''} for batch in batchs]



def get_time():
    """
    Obtiene la fecha y hora actual desde un servidor NTP
    """
    servidores_ntp = ['ar.pool.ntp.org', 'south-america.pool.ntp.org']
    c = ntplib.NTPClient()
   
    while True:
        for servidor_ntp in servidores_ntp:
            try:
                response = c.request(servidor_ntp)
                
                date = dt.datetime.fromtimestamp(response.tx_time)
                format_date = date.strftime('%d/%m/%Y  %H:%M')
                return format_date
            except ntplib.NTPException as e:
                # Si ocurre una excepción, intenta con el siguiente servidor NTP
                print(f"Error de conexión con el servidor NTP {servidor_ntp}: {e}")
        
        # Si todos los servidores fallan, obtén la fecha del sistema y sale del bucle
        system_date = dt.datetime.now()
        format_system_date = system_date.strftime('%d/%m/%Y  %H:%M')
        print(f"Error: no se pudo obtener la fecha y hora desde NTP, utilizando la fecha del sistema: {format_system_date}")
        return format_system_date


"""

def get_time():
   
    servidores_ntp = ['ar.pool.ntp.org', 'south-america.pool.ntp.org']
    c = ntplib.NTPClient()
    date = None
    retries = 0
    while date is None and retries < 3:  # Intentamos un máximo de 3 veces
        for servidor_ntp in servidores_ntp:
            try:
                response = c.request(servidor_ntp, timeout=2.0)  # Añadimos un control de tiempo de espera
                date = datetime.datetime.fromtimestamp(response.tx_time)
                format_date = (date.strftime('%d/%m/%Y  %H:%M'))
                break
            except ntplib.NTPException as e:
                logging.error(f"Error de conexión con el servidor NTP {servidor_ntp}: {e}")
                continue  # Intentamos con el siguiente servidor NTP
            except Exception as e:
                logging.error(f"Error desconocido: {e}")
                continue  # Intentamos con el siguiente servidor NTP
        else:
            # Si llega aquí significa que no se pudo obtener una fecha válida
            logging.warning("Error: no se pudo obtener la fecha y hora actual")
            return None
           
    return format_date
"""
   

def get_new_date(current, days_to_add):
    """
    Devuelve una fecha y hora actualizada sumando una cierta cantidad de días a la fecha actual
    """
    current_date = current
    date_obj = dt.datetime.strptime(current_date, '%d/%m/%Y %H:%M')
    new_date_obj = date_obj + timedelta(days= int(days_to_add))
    new_date = new_date_obj.strftime('%d/%m/%Y  %H:%M')
    return new_date


def barrel_exist(id, ref):
    
    barrel = ref.where('id', '==', id).get()

    for doc in barrel:
        doc_ref = ref.document(f'{doc.id}')
 
    return doc, doc_ref
  
    
    
def get_barrels_data(barrel_id, ref):
    doc_ref = ref.document(barrel_id)
    doc = doc_ref.get()

    if doc.exists:
        return doc.to_dict()
    else:
        return None
    
def get_data_by_id(id, barrels_ref):
    href = barrels_ref.where('id', '==', id).get()
    barrel_doc = href[0]
    barrel_data = barrel_doc.to_dict()
    return barrel_data



def quick_update(id, location, barrels_ref):

    data = barrels_ref.where('id', '==', id).get()
    data_ref = data[0].reference
    data_ref.update({'location': location})




def quick_update_by_condition(id, condition, barrels_ref):

    data = barrels_ref.where('id', '==', id).get()
    data_ref = data[0].reference
    data_ref.update({'condition': condition})



def get_style_of_batch(s):
    parts = s.split(',')
    if len(parts) > 1:
        return parts[1].strip()
    else:
        return ''



def validate_field(value):
    allowed_characters = set('abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789-')
    for char in value:
        if char not in allowed_characters:
            return False
    return True




def cycle_counter(ref):
    count = 0
    # Recorremos la lista de acciones en 'action'
    for action in ref['action']:
        # Veriicamos si la palabra "Cambia" esta en action 
        if 'Cambia' in action:
            count +=1
    
    return count




def average_time_state(barrel):
    estados = barrel['conditions_history']
    fechas = barrel['date']
    
    # Convierte las fechas de entrada en objetos datetime
    fechas_datetime = [dt.datetime.strptime(fecha, '%d/%m/%Y  %H:%M') for fecha in fechas]
    
    tiempo_en_estado = {}
    
    for i in range(len(estados)):
        estado_actual = estados[i]
        fecha_actual = fechas_datetime[i]
        
        if estado_actual not in tiempo_en_estado:
            tiempo_en_estado[estado_actual] = {'tiempo_total': timedelta(0), 'contador': 0}
        
        if i < len(estados) - 1:
            estado_siguiente = estados[i + 1]
            fecha_siguiente = fechas_datetime[i + 1]
            
            # Calcula la diferencia de tiempo entre los estados
            tiempo_en_estado[estado_actual]['tiempo_total'] += (fecha_siguiente - fecha_actual)
            tiempo_en_estado[estado_actual]['contador'] += 1
    
    # Calcula el tiempo promedio en cada estado en días, horas y minutos
    tiempo_promedio_por_estado = {}
    for estado, datos_tiempo in tiempo_en_estado.items():
        tiempo_total = datos_tiempo['tiempo_total']
        contador = datos_tiempo['contador']
        
        # Calcula el tiempo promedio en días, horas y minutos
        tiempo_promedio_dias = tiempo_total.days / contador if contador > 0 else 0
        tiempo_promedio_segundos = tiempo_total.seconds / contador if contador > 0 else 0
        tiempo_promedio_horas = tiempo_promedio_segundos // 3600
        tiempo_promedio_minutos = (tiempo_promedio_segundos % 3600) // 60
        tiempo_promedio_por_estado[estado] = (int(tiempo_promedio_dias), int(tiempo_promedio_horas), int(tiempo_promedio_minutos))
    
    return tiempo_promedio_por_estado




def verify_cycle_counter(id, barrels_ref, ref):

    href = barrels_ref.where('id', '==', id).stream()
    barrel_docs = list(href)
    
    if len(barrel_docs) == 1:
        # Obtén el primer documento de la lista
        barrel_doc = barrel_docs[0].to_dict()

        cc = cycle_counter(ref)
        
        # Verifica si el valor de change_cycle es igual a 250
        if cc > 250:
            # Elimina el primer elemento de conditions_history y date
            barrel_doc['conditions_history'].pop(0)
            barrel_doc['date'].pop(0)
            barrel_doc['action'].pop(0)
            barrel_doc['user'].pop(0)
            # Actualiza el documento en Firestore
            barrels_ref.document(barrel_docs[0].id).update(barrel_doc)
        
            session['success_msg'] ="Documento actualizado en Firestore"
       
        if len(barrel_doc['batchs_history']) > 250:
            barrel_doc['batchs_history'].pop(0)
            barrel_doc['batchs_date'].pop(0)

    else:
        session['error_msg'] ="El documento no existe en Firestore o hay múltiples documentos con el mismo ID"





def draw_bar_graphics(estado_contador):
    plt.close('all')
    porcentajes = 0

    # Colores personalizados para las barras
    color_cantidad = '#63cb9a'  # Verde
    color_porcentaje = '#4579b2'  # Azul

    # Calcular el total de elementos en el diccionario
    total_elementos = sum(estado_contador.values())
    # Recorrer el diccionario y calcular el porcentaje de cada estado
    porcentajes = {estado: (cantidad / total_elementos) * 100 for estado, cantidad in estado_contador.items()}
   

    estados = ['Sucio', 'Vacío', 'En servicio', 'En almacenamiento']


     # Crear gráfico de barras apiladas con diseño mejorado
    fig, ax = plt.subplots(figsize=(10, 6))

    # Barra para la cantidad de estados
    bar1 = ax.bar(estados, [estado_contador[estado] for estado in estados], color=color_cantidad, edgecolor='black', label='Cantidad de Estados')

    # Barra para el porcentaje
    bar2 = ax.bar(estados, [porcentajes[estado] for estado in estados], bottom=[estado_contador[estado] for estado in estados], color=color_porcentaje, edgecolor='black', label='Frecuencia de cambios')

    ax.set_ylabel('Cantidad / Porcentaje', fontsize=12, fontweight='bold')
    ax.set_title('Estados y Frecuencia', fontsize=14, fontweight='bold')

    # Agregar etiquetas con valores numéricos
    for rect, valor in zip(bar1 + bar2, [estado_contador[estado] for estado in estados] + [porcentajes[estado] for estado in estados]):
        height = rect.get_height()
        if rect in bar2:  # Mostrar solo el número decimal y el signo de porcentaje en las etiquetas de porcentaje
            ax.annotate('{:.1f}%'.format(valor),
                        xy=(rect.get_x() + rect.get_width() / 2, height),
                        xytext=(0, -15),  # offset vertical
                        textcoords="offset points",
                        ha='center', va='center',
                        fontsize=10, fontweight='bold', color='white')  # Estilo mejorado para porcentaje
        else:  # Mostrar solo el valor de cantidad en las etiquetas de cantidad
            ax.annotate('{}'.format(valor),
                        xy=(rect.get_x() + rect.get_width() / 2, height),
                        xytext=(0, 5),  # offset vertical
                        textcoords="offset points",
                        ha='center', va='center',
                        fontsize=10, fontweight='bold')  # Estilo mejorado para cantidad

    ax.legend(fontsize=10, loc='upper left')

    # Ajustes adicionales para mejorar el diseño
    ax.spines['top'].set_visible(False)
    ax.spines['right'].set_visible(False)
    ax.spines['left'].set_linewidth(0.5)
    ax.spines['bottom'].set_linewidth(0.5)
    ax.tick_params(axis='both', which='both', length=0)

    # Guardar el gráfico en un objeto BytesIO
    img_buf = BytesIO()
    plt.savefig(img_buf, format='png', bbox_inches='tight')
    img_buf.seek(0)

    # Convertir la imagen a base64 para incrustarla en el template
    img_base64 = base64.b64encode(img_buf.getvalue()).decode('utf-8')


    return img_base64




def count_barrels(barrels, condition):
    count = 0
    liters = 0
    gasified = {"En servicio": {"gasified": 0, "not_gasified": 0},
            "En almacenamiento": {"gasified": 0, "not_gasified": 0}}
  
    
    for barrel in barrels:
        if barrel.get('condition') == condition:
            count += 1
            liters += int(barrel.get('liters'))

                  
            if condition in gasified:
                if barrel.get('gasified') == 'SI':
                    gasified[condition]['gasified'] +=1
                     
                else:
                    gasified[condition]['not_gasified'] +=1
            else:
                gasified[condition] = {'gasified': 0, 'not_gasified': 0}
                if barrel.get('gasified') == 'SI':
                    gasified[condition]['gasified'] +=1
                else:
                    gasified[condition]['not_gasified'] +=1
         
    return count, liters, gasified



def process_barrels(barrels_ref, barrels=0):
    if barrels == 0:
        barrels = get_barrels(barrels_ref)
    if not barrels:
        return (0, 0, 0, 0, 0, 0, 0, 0), (0, 0, 0, 0, 0, 0), [], [], [], [], []
    else: 

        service, service_liters, gas_service = count_barrels(barrels, 'En servicio')
        stored, stored_liters, gas_stored = count_barrels(barrels, 'En almacenamiento')
        empty, empty_liters, gas_empty = count_barrels(barrels, 'Vacío')
        dirt, dirt_liters, gas_dirty = count_barrels(barrels, 'Sucio')

        count = service + stored + empty + dirt
        util_barrels = service + stored
        total_liters = service_liters + stored_liters + empty_liters + dirt_liters
        net_liters = service_liters + stored_liters
   
        #count_gasified = {gas_service['En servicio']['gasified'], gas_stored['En almacenamiento']['gasified']}
        #count_no_gasified = {gas_service['En servicio']['not_gasified'], gas_stored['En almacenamiento']['not_gasified']}
        total_gasified = gas_service['En servicio']['gasified'] + gas_stored['En almacenamiento']['gasified']
        total_no_gasified = gas_service['En servicio']['not_gasified'] + gas_stored['En almacenamiento']['not_gasified']
        

        list_barrel = [{'id': barrel.get('id'), 
                        'liters': barrel.get('liters'), 
                        'serial': barrel.get('serial'),
                        'batch': barrel.get('batch'),
                        'batchs_history': barrel.get('batchs_history'),
                        'batchs_date': barrel.get('batchs_date'),
                        'condition': barrel.get('condition'), 
                        'style': barrel.get('style'),
                        'location': barrel.get('location'), 
                        'gasified': barrel.get('gasified'),
                        'conditions_history': barrel.get('conditions_history'),
                        'date': barrel.get('date'),
                        'user': barrel.get('user')}
                    for barrel in barrels]

        barrel_count_style = count_barrels_by(barrels, by_style=True)
        barrel_count_batch = count_barrels_by(barrels, by_style=False)
        barrel_location = count_barrels_by_location(barrels)
        service_location= count_barrels_by_location_and_condition(barrels, 'En servicio')
        storage_location = count_barrels_by_location_and_condition(barrels, 'En almacenamiento')
        dirty_location = count_barrels_by_location_and_condition(barrels, 'Vacío')
        empty_location = count_barrels_by_location_and_condition(barrels, 'Sucio')
        location_condition = [service_location, storage_location, dirty_location, empty_location]
    return (service, stored, empty, dirt, count, util_barrels, total_gasified,  total_no_gasified), (service_liters, stored_liters,
                                                   empty_liters, dirt_liters, total_liters, net_liters), list_barrel, barrel_count_style, barrel_count_batch, barrel_location, location_condition





def count_barrels_by_location(barrels):
    barrel_location_count = {}

    for barrel in barrels:
        location = barrel.get('location')
        if location != 'Fábrica' and location != 'Bar':
            location = 'Cliente' 
        liters = int(barrel.get('liters'))

        if location in barrel_location_count:
            barrel_location_count[location]['count'] +=1
            barrel_location_count[location]['liters'] += liters
        
        else:
            barrel_location_count[location]={
                'count' : 1,
                'liters' : liters
                
            }
    return barrel_location_count



def count_barrels_by_location_and_condition(barrels, condition):
    barrel_location_condition_count = {}

    for barrel in barrels:
        location = barrel.get('location')
        if location != 'Fábrica' and location != 'Bar':
            location = 'Cliente' 
        liters = int(barrel.get('liters'))

        if barrel.get('condition') == condition:
            if location in barrel_location_condition_count:
                barrel_location_condition_count[location]['count'] +=1
                barrel_location_condition_count[location]['liters'] += liters
            
            else:
                barrel_location_condition_count[location]={
                    'count' : 1,
                    'liters' : liters
                    
                }
    return barrel_location_condition_count



def count_barrels_by_style(barrels):
    barrel_count = {}
    for barrel in barrels:
        style = barrel.get('style')
        if style is None or style == '':
            continue 
        liters = int(barrel.get('liters'))
        gasified = barrel.get('gasified') == 'SI'
        in_service = barrel.get('condition') in ['En servicio']
        in_storage = barrel.get('condition') in ['En almacenamiento']
        
        if style not in barrel_count:
            barrel_count[style] = {
                'count': 0,
                'liters': 0,
                'net_count': 0,
                'net_liters': 0,
                'gasified': 0,
                'gasified_liters': 0,
                'not_gasified': 0,
                'not_gasified_liters': 0,
                'service_style': 0,
                'storage_style': 0
            }
        
        barrel_count[style]['count'] += 1
        barrel_count[style]['liters'] += liters
        
      
        if in_service:
            barrel_count[style]['service_style'] += 1
        
        if in_storage:
            barrel_count[style]['storage_style'] += 1
        
        if gasified:
            barrel_count[style]['gasified'] += 1
            barrel_count[style]['gasified_liters'] += liters
        
        if not gasified:
            barrel_count[style]['not_gasified'] += 1
            barrel_count[style]['not_gasified_liters'] += liters
          
    return barrel_count



def count_barrels_by_batch(barrels):
    barrel_count = {}
    for barrel in barrels:
        batch = barrel.get('batch')
        if batch is None or batch == '':
            continue

        liters = int(barrel.get('liters'))
        gasified = barrel.get('gasified') == 'SI'
        in_service = barrel.get('condition') in ['En servicio']
        in_storage = barrel.get('condition') in ['En almacenamiento']

        if batch not in barrel_count:
            barrel_count[batch] = {
                'count': 0,
                'liters': 0,
                'net_count': 0,
                'net_liters': 0,
                'gasified': 0,
                'gasified_liters': 0,
                'not_gasified': 0,
                'not_gasified_liters': 0,
                'service_batch': 0,
                'storage_batch': 0
            }
        barrel_count[batch]['count'] += 1
        barrel_count[batch]['liters'] += liters
        
      
        if in_service:
            barrel_count[batch]['service_batch'] += 1
        
        if in_storage:
            barrel_count[batch]['storage_batch'] += 1
        
        if gasified:
            barrel_count[batch]['gasified'] += 1
            barrel_count[batch]['gasified_liters'] += liters
        
        if not gasified:
            barrel_count[batch]['not_gasified'] += 1
            barrel_count[batch]['not_gasified_liters'] += liters
          
    return barrel_count


def count_barrels_by(barrels, by_style=True):
    barrel_count = {}

    for barrel in barrels:
        category = barrel.get('style') if by_style else barrel.get('batch')

        if category is None or category == '':
            continue

        liters = int(barrel.get('liters'))
        gasified = barrel.get('gasified') == 'SI'
        in_service = barrel.get('condition') in ['En servicio']
        in_storage = barrel.get('condition') in ['En almacenamiento']

        if category not in barrel_count:
            barrel_count[category] = {
                'count': 0,
                'liters': 0,
                'net_count': 0,
                'net_liters': 0,
                'gasified': 0,
                'gasified_liters': 0,
                'not_gasified': 0,
                'not_gasified_liters': 0,
                'service_category': 0,
                'storage_category': 0
            }

        barrel_count[category]['count'] += 1
        barrel_count[category]['liters'] += liters

        if in_service:
            barrel_count[category]['service_category'] += 1

        if in_storage:
            barrel_count[category]['storage_category'] += 1

        if gasified:
            barrel_count[category]['gasified'] += 1
            barrel_count[category]['gasified_liters'] += liters

        if not gasified:
            barrel_count[category]['not_gasified'] += 1
            barrel_count[category]['not_gasified_liters'] += liters

    return barrel_count


def calculate_percentage(response):
    if not response[4]:
        return [0] * 4
    else:
        total = int(response[4])
        return [round(int(r) / total * 100) for r in response[:4]]


def percentage(barrels_ref):
    response1, response2, list_barrel, count_barrels, barrel_count_batch, barrel_location, location_condition = process_barrels(barrels_ref)
    p1, p2, p3, p4 = calculate_percentage(response1)
    l1, l2, l3, l4 = calculate_percentage(response2)
    return [p1, p2, p3, p4, l1, l2, l3, l4]

